package com.epam.gateway.filter;


import java.util.List;
import java.util.function.Predicate;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

@Component
public class RouteValidator {
	
	private RouteValidator() {}

    public static final List<String> endPoints = List.of(
            "/api/gym/trainees/registration",
            "/api/gym/trainers/registration",
            "/eureka",
            "/api/auth/token",
            "/api/auth/validate"
    );

    public Predicate <ServerHttpRequest> isSecured =
            request -> endPoints
                    .stream()
                    .noneMatch(uri -> request.getURI().getPath().contains(uri));
   
}
