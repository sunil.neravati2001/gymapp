package com.epam.gym.controller;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gym.dto.TrainingDTO;
import com.epam.gym.dto.response.TrainingResponse;
import com.epam.gym.service.TrainingService;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/gym/training")
@Slf4j
public class TrainingController {

	private final TrainingService trainingService;

	@PostMapping("/add")
	public ResponseEntity<Void> addTraining(@RequestBody @Valid TrainingDTO trainingDto) throws JsonProcessingException {
		log.info("Entered add training method, TrainingDTO : {}",trainingDto);
		trainingService.addTraining(trainingDto);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/trainee")
	public List<TrainingResponse> getTraineeTrainings(@RequestParam String username,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date periodFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date periodTo,
			@RequestParam(required = false) String trainerName, @RequestParam(required = false) String trainingType) {
		log.info("Entered get Trainee Trainings method, username : {}",username);
		return trainingService.getTraineeTrainings(username, periodFrom, periodTo, trainerName, trainingType);
	}

	@GetMapping("/trainer")
	public List<TrainingResponse> getTrainerTrainings(@RequestParam String username,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date periodFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date periodTo,
			@RequestParam(required = false) String traineeName

	) {
		log.info("Entered get TraineR Trainings method, username : {}",username);
		return trainingService.getTrainerTrainings(username, periodFrom, periodTo, traineeName);
	}
}
