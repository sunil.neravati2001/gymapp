package com.epam.gym.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gym.dto.TraineeDTO;
import com.epam.gym.dto.UpdateTraineeDTO;
import com.epam.gym.dto.response.RegistrationResponse;
import com.epam.gym.dto.response.TraineeProfileResponse;
import com.epam.gym.dto.response.TrainerResponse;
import com.epam.gym.dto.response.UpdatedTraineeResponse;
import com.epam.gym.service.TraineeService;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/gym/trainees")
@Slf4j
public class TraineeController {

	@Autowired
	private TraineeService traineeService;

	@PostMapping("/registration")
	public ResponseEntity<RegistrationResponse> addTrainee(@RequestBody @Valid TraineeDTO traineeDTO) throws JsonProcessingException {
		log.info("Entered add Trainee method, TraineeDTO : {}",traineeDTO);
		return new ResponseEntity<>(traineeService.traineeRegistration(traineeDTO), HttpStatus.CREATED);
	}

	@GetMapping("/profile/{username}")
	public ResponseEntity<TraineeProfileResponse> getTraineeProfileDetails(@PathVariable String username) {
		log.info("Entered get Trainee Profile details method, username : {} ",username);
		return new ResponseEntity<>(traineeService.getTraineeProfileDetails(username), HttpStatus.OK);
	}

	@PutMapping("/update")
	public ResponseEntity<UpdatedTraineeResponse> updateProfileDetails(
			@RequestBody @Valid UpdateTraineeDTO updateTraineeDTO) throws JsonProcessingException {
		log.info("Entered update profie details method, updateTraineeDTO : {}",updateTraineeDTO);
		return new ResponseEntity<>(traineeService.updateTraineeDetails(updateTraineeDTO), HttpStatus.OK);
	}

	@DeleteMapping("/delete/{username}")
	public ResponseEntity<Void> deleteTrainee(@PathVariable String username) {
		log.info("Entered delete trainee method, username : {}",username);
		traineeService.deleteTrainee(username);
		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	@GetMapping("/{username}/not-assigned-trainers")
	public ResponseEntity<List<TrainerResponse>> notAssignedTrainers(@PathVariable String username){
		log.info("Entered not assigned trainers method, username : {}",username);
		return new ResponseEntity<>(traineeService.fetchNotAssignedTrainers(username),HttpStatus.OK);
	}
	
	@PutMapping("/update/{username}/trainer")
	public ResponseEntity<List<TrainerResponse>> updateTrainerDetails(@PathVariable String username,
			@RequestParam List<String> trainerNames) {

		return new ResponseEntity<>(traineeService.updateTrainers
				(username, trainerNames), HttpStatus.OK);
	}
}
