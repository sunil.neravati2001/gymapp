package com.epam.gym.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gym.service.LoginService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/gym")
@Slf4j
public class LoginController {

	private final LoginService loginService;

	@GetMapping("/login")
	public ResponseEntity<Void> userLogin(@RequestParam String userName, @RequestParam String password) {
		log.info("Entered user login method, username : {}, password: {}",userName,password);
		loginService.login(userName, password);
		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	@GetMapping("/updatepassword")
	public ResponseEntity<Void> updatePassword(@RequestParam String userName, @RequestParam String oldPassword,@RequestParam String newPassword) {
		log.info("Entered update password method, username : {}, oldpassword : {}, newpassword : {}",userName,oldPassword,newPassword);
		loginService.changePassword(userName, oldPassword,newPassword);
		return new ResponseEntity<>(HttpStatus.OK);

	}
}
