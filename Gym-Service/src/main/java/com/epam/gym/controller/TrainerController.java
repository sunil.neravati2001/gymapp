package com.epam.gym.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gym.dto.TrainerDTO;
import com.epam.gym.dto.UpdateTrainerDTO;
import com.epam.gym.dto.response.RegistrationResponse;
import com.epam.gym.dto.response.TrainerProfileResponse;
import com.epam.gym.dto.response.UpdatedTrainerResponse;
import com.epam.gym.service.TrainerService;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/gym/trainers")
@Slf4j
public class TrainerController {

	@Autowired
	private final TrainerService trainerService;

	@PostMapping("/registration")
	public ResponseEntity<RegistrationResponse> addTrainee(@RequestBody @Valid TrainerDTO trainerDTO) throws JsonProcessingException {
		log.info("Entered add trainee method, Trainer DTO is {}",trainerDTO);
		return new ResponseEntity<>(trainerService.trainerRegistration(trainerDTO), HttpStatus.CREATED);
	}

	@GetMapping("/profile/{username}")
	public ResponseEntity<TrainerProfileResponse> getTrainerProfileDetails(@PathVariable String username) {
		log.info("Entered get trainer profile details method, username : {}",username);
		return new ResponseEntity<>(trainerService.getTrainerProfileDetails(username), HttpStatus.OK);
	}

	@PutMapping("/update")
	public ResponseEntity<UpdatedTrainerResponse> updateProfileDetails(
			@RequestBody @Valid UpdateTrainerDTO updateTrainerDTO) throws JsonProcessingException {
		log.info("Entered update trainer dto method, updatedTrainerDTO : {}",updateTrainerDTO);
		return new ResponseEntity<>(trainerService.updateTrainerDetails(updateTrainerDTO), HttpStatus.OK);
	}
	
}
