package com.epam.gym.dao;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;

@Repository
public interface TrainerRepository extends JpaRepository<Trainer, Integer> {

	Optional<Trainer> findByUserUsername(String username);
	Set<Trainer> findByUserUsernameIn(List<String> trainerNames);
	List<Trainer> findByTraineesNotContaining(Trainee trainee);

}
