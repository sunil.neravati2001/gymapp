package com.epam.gym.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.epam.gym.dto.response.TrainingResponse;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.Training;

@Repository
public interface TrainingRepository extends JpaRepository<Training, Integer> {

    @Modifying
    @Query("DELETE FROM Training t WHERE t.trainee = :trainee")
    void deleteByTrainee(@Param("trainee") Trainee trainee);

    @Query("SELECT new com.epam.gym.dto.response.TrainingResponse(t.trainingName, t.trainingDate, tt.trainingTypeName, t.trainingDuration,trr.user.username) "
            + "FROM Training t " + "JOIN t.trainee tr " + "JOIN t.trainer trr " + "JOIN t.trainingType tt "
            + "WHERE tr.user.username = :username " + "AND (:periodFrom IS NULL OR t.trainingDate >= :periodFrom) "
            + "AND (:periodTo IS NULL OR t.trainingDate <= :periodTo) "
            + "AND (:trainerName IS NULL OR trr.user.firstName || ' ' || trr.user.lastName || ' ' || trr.user.username LIKE %:trainerName%) "
            + "AND (:trainingType IS NULL OR tt.trainingTypeName = :trainingType)")
    List<TrainingResponse> getTraineeTrainings(String username, Date periodFrom, Date periodTo, String trainerName,
                                               String trainingType);

    @Query("SELECT new com.epam.gym.dto.response.TrainingResponse(t.trainingName, t.trainingDate, tt.trainingTypeName, t.trainingDuration,tr.user.username) "
            + "FROM Training t " + "JOIN t.trainee tr " + "JOIN t.trainer trr " + "JOIN t.trainingType tt "
            + "WHERE trr.user.username = :username " + "AND (:periodFrom IS NULL OR t.trainingDate >= :periodFrom) "
            + "AND (:periodTo IS NULL OR t.trainingDate <= :periodTo) "
            + "AND (:traineeName IS NULL OR tr.user.firstName || ' ' || tr.user.lastName || ' ' || tr.user.username LIKE %:traineeName%) ")
    List<TrainingResponse> getTrainerTrainings(String username, Date periodFrom, Date periodTo, String traineeName);

    @Query(value = "SELECT t.id, t.trainee_id, t.trainer_id, t.training_date, t.training_duration, t.training_name, t.training_type_id "
            + "FROM training t " + "JOIN trainer tr ON t.trainer_id = tr.id " + "JOIN user u ON tr.user_id = u.id "
            + "WHERE u.username = :trainerUsername "
            + "AND (t.training_date < :givenDate OR DATE(t.training_date) = DATE(:givenDate)) "
            + "AND MONTH(t.training_date) = MONTH(:givenDate) "
            + "AND YEAR(t.training_date) = YEAR(:givenDate)", nativeQuery = true)
    List<Training> findActiveTrainingByTrainerAndDate(String trainerUsername, Date givenDate);

    Optional<Training> findByTrainerAndTraineeAndTrainingDate(Trainer trainer, Trainee trainee, Date trainingDate);

    List<Training> findByTrainerAndTrainee(Trainer trainer, Trainee trainee);

}
