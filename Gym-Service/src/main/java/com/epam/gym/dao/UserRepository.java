package com.epam.gym.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.gym.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	List<User> findByUsernameStartsWith(String username);
	Optional<User> findByUsernameAndPassword(String userName, String password);
	boolean existsByUsernameAndPassword(String userName, String password);
	Optional<User> findByUsername(String username);
	void deleteByUsername(String username);
	
}
