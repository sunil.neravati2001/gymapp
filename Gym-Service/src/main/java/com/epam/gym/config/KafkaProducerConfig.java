package com.epam.gym.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaProducerConfig {
	
    @Bean
    public NewTopic createNotificationTopic(){
        return TopicBuilder.name("notification-1").build();
    }
    
    @Bean
    public NewTopic createReportingTopic() {
    	return TopicBuilder.name("reporting").build();
    }

}
