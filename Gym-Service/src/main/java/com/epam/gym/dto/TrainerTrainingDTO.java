package com.epam.gym.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TrainerTrainingDTO {

	@NotBlank(message = "username should not be null")
	private String username;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date periodFrom;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date periodTo;

	private String traineeName;


}
