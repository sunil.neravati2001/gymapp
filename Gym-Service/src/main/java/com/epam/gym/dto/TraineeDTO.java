package com.epam.gym.dto;

import java.util.Date;


import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TraineeDTO {

	@NotBlank(message = "FirstName should not be blank")
	private String firstName;

	@NotBlank(message = "LastName should not be blank")
	private String lastName;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfBirth;

	private String address;
	
	@Email
	private String email;
}
