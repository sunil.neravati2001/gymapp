package com.epam.gym.dto;

import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class TrainingTypeDTO {
	
	@Pattern(regexp = "^(?i)(fitness|yoga|Zumba|stretching|resistance)$", message = "Invalid fitness type")
	private String trainingTypeName;

}
