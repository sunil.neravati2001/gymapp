package com.epam.gym.dto.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class UpdatedTrainerResponse {

	
	private String firstName;

	private String lastName;
	
	private String username;
	
	private String specialization;
	
	private String email;
	
	private Boolean isActive;
	
	private List<TraineeResponse> trainees;
}
