package com.epam.gym.dto.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class TrainerProfileResponse {

	private String firstName;

	private String lastName;
	
	private String specialization;
	
	private Boolean isActive;
	
	private List<TraineeResponse> trainees;
}
