package com.epam.gym.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class TraineeResponse {

	private String firstName;

	private String lastName;

	private String username;
}
