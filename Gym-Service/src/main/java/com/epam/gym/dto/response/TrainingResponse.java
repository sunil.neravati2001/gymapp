package com.epam.gym.dto.response;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TrainingResponse {

	private String trainingName;
	private Date trainingDate;
	private String trainingType;
	private int trainingDuration;
	private String username;

}
