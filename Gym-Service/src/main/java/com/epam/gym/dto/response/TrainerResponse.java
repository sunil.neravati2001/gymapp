package com.epam.gym.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TrainerResponse {
	
	private String firstName;
	private String lastName;
	private String username;
	private String specialization;

}
