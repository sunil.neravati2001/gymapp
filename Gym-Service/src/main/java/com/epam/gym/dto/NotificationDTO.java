package com.epam.gym.dto;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class NotificationDTO {
	
	private List<String> toEmails;
	
	private List<String> ccEmails;
	
	private Map<String,String> body;
	
	private String subject;

}
