package com.epam.gym.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TrainingDTO {

	@NotBlank(message = "trainee username should not be empty")
	private String traineeUsername;

	@NotBlank(message = "trainer username should not be empty")
	private String trainerUsername;

	@NotBlank(message = "training name should not be empty")
	private String trainingName;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date trainingDate;

	@Pattern(regexp = "^(?i)(fitness|yoga|Zumba|stretching|resistance)$", message = "Invalid fitness type")
	private String trainingType;

	@Min(value = 1,message = "Enter valid duration")
	private int duration;

}
