package com.epam.gym.dto.response;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TraineeProfileResponse {

	private String firstName;
	
	private String lastName;

	private Date dateOfBirth;

	private String address;
	
	private boolean isActive;
	
	private List<TrainerResponse> trainers;
	
}
