package com.epam.gym.service;

import java.util.Date;
import java.util.List;

import com.epam.gym.dto.TrainingDTO;
import com.epam.gym.dto.response.TrainingResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface TrainingService {

	 void addTraining(TrainingDTO trainingDTO) throws JsonProcessingException;
	
	 List<TrainingResponse> getTraineeTrainings(String username, Date periodFrom,Date periodTo,String trainerName, String trainingType);

	 List<TrainingResponse> getTrainerTrainings(String username, Date periodFrom,Date periodTo,String traineeName);
	
}
