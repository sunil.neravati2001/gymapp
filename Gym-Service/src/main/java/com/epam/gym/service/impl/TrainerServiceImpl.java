package com.epam.gym.service.impl;

import java.util.List;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gym.dao.TrainerRepository;
import com.epam.gym.dao.TrainingTypeRepository;
import com.epam.gym.dao.UserRepository;
import com.epam.gym.dto.NotificationDTO;
import com.epam.gym.dto.TrainerDTO;
import com.epam.gym.dto.UpdateTrainerDTO;
import com.epam.gym.dto.response.RegistrationResponse;
import com.epam.gym.dto.response.TraineeResponse;
import com.epam.gym.dto.response.TrainerProfileResponse;
import com.epam.gym.dto.response.UpdatedTrainerResponse;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.TrainingType;
import com.epam.gym.entity.User;
import com.epam.gym.exception.GymException;
import com.epam.gym.service.TrainerService;
import com.epam.gym.utility.CredentialGenerator;
import com.epam.gym.utility.NotificationConverter;
import com.epam.gym.utility.RegistrationConverter;
import com.epam.gym.utility.TraineeConverter;
import com.epam.gym.utility.TrainerConverter;
import com.epam.gym.utility.UserConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Service
@Transactional
@Slf4j
public class TrainerServiceImpl implements TrainerService {

	private final TrainerRepository trainerRepository;
	private final UserRepository userRepository;
	private final TrainingTypeRepository trainingTypeRepository;
	private final NotificationProducer notificationProducer;
	private final PasswordEncoder encoder;
	private final CredentialGenerator generator;
	private final TraineeConverter traineeConverter;
	private final NotificationConverter notificationConverter;
	private final RegistrationConverter registrationConverter;
	private final UserConverter userConverter;
	private final TrainerConverter trainerConverter;
	
	@Override
	public RegistrationResponse trainerRegistration(TrainerDTO trainerDTO) throws JsonProcessingException {
		
		log.info("Entered trainer Registration method, TrainerDTO : {}");

		String name = trainerDTO.getFirstName().toLowerCase() + trainerDTO.getLastName().toLowerCase();
		List<String> usernames = userRepository.findByUsernameStartsWith(name).stream().map(User::getUsername).toList();
		String username = generator.generateUniqueUsername(trainerDTO.getFirstName(), trainerDTO.getLastName(),
				usernames);

		String password = generator.generatePassword();
		User user = userConverter.getUserFromTrainerDTO(trainerDTO, username, encoder.encode(password));

		Trainer trainer = new Trainer();
		trainer.setUser(user);

		TrainingType trainingType = trainingTypeRepository.findByTrainingTypeName(trainerDTO.getSpecialization())
				.orElseGet(() -> {
					TrainingType type = new TrainingType();
					type.setTrainingTypeName(trainerDTO.getSpecialization());
					return type;
				});

		trainer.setSpecialization(trainingType);
		trainerRepository.save(trainer);
		
		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(user,password);
		notificationProducer.sendNotification(notificationDTO);
		
		return registrationConverter.getRegistrationResponse(username, password);

	}

	@Override
	public TrainerProfileResponse getTrainerProfileDetails(String username) {
		
		log.info("Entered get Trainer Profile details method, username : {}",username);

		Trainer trainer = trainerRepository.findByUserUsername(username)
				.orElseThrow(() -> new GymException("trainer not found"));

		User user = trainer.getUser();
		List<TraineeResponse> traineeProfiles = trainer.getTrainees().stream().map(trainee -> traineeConverter.getTraineeResponse(trainee.getUser())).toList();
		return trainerConverter.getTrainerProfileResponse(trainer, user, traineeProfiles);

	}

	@Override
	public UpdatedTrainerResponse updateTrainerDetails(UpdateTrainerDTO updateTrainerDTO) throws JsonProcessingException {
		
		log.info("Entered update Trainer details method, updateTrainerDTO : {}",updateTrainerDTO);

		Trainer trainer = trainerRepository.findByUserUsername(updateTrainerDTO.getUsername())
				.orElseThrow(() -> new GymException("trainer not found"));

		User user = userConverter.getUserFromUpdateTrainerDTO(updateTrainerDTO, trainer.getUser());
		List<TraineeResponse> traineeProfiles = trainer.getTrainees().stream().map(trainee -> traineeConverter.getTraineeResponse(trainee.getUser())).toList();

		UpdatedTrainerResponse updatedTrainerResponse = trainerConverter.getUpdatedResponse(trainer, user, traineeProfiles);

		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(trainer);
		notificationProducer.sendNotification(notificationDTO);
		
		return updatedTrainerResponse;

	}


}
