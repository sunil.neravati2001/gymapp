package com.epam.gym.service;

import com.epam.gym.dto.TrainerDTO;
import com.epam.gym.dto.UpdateTrainerDTO;
import com.epam.gym.dto.response.RegistrationResponse;
import com.epam.gym.dto.response.TrainerProfileResponse;
import com.epam.gym.dto.response.UpdatedTrainerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface TrainerService {
	
	 RegistrationResponse trainerRegistration(TrainerDTO trainerDTO) throws JsonProcessingException;
	 TrainerProfileResponse getTrainerProfileDetails(String username);
	 UpdatedTrainerResponse updateTrainerDetails(UpdateTrainerDTO updateTrainerDTO) throws JsonProcessingException;
			
}
