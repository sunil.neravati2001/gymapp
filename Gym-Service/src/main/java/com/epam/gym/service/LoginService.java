package com.epam.gym.service;

public interface LoginService {
	
	 void login(String userName,String password);
	
	 void changePassword(String userName,String oldPassword,String newPassword);

}
