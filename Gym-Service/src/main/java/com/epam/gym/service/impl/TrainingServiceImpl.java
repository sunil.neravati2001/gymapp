package com.epam.gym.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.gym.dao.TraineeRepository;
import com.epam.gym.dao.TrainerRepository;
import com.epam.gym.dao.TrainingRepository;
import com.epam.gym.dao.UserRepository;
import com.epam.gym.dto.NotificationDTO;
import com.epam.gym.dto.TrainingDTO;
import com.epam.gym.dto.TrainingReportDTO;
import com.epam.gym.dto.response.TrainingResponse;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.User;
import com.epam.gym.exception.GymException;
import com.epam.gym.service.TrainingService;
import com.epam.gym.utility.NotificationConverter;
import com.epam.gym.utility.TrainingConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class TrainingServiceImpl implements TrainingService {

	private final TrainingRepository trainingRepository;
	private final TrainerRepository trainerRepository;
	private final TraineeRepository traineeRepository;
	private final UserRepository userRepository;
	private final NotificationProducer notificationProducer;
	private final ReportingProducer reportingProducer ;
	private final NotificationConverter notificationConverter;
	private final TrainingConverter trainingConverter;

	@Override
	public void addTraining(TrainingDTO trainingDTO) throws JsonProcessingException {
		
		log.info("Entered add training method, TrainingDTO : {}",trainingDTO);

		Trainee trainee = traineeRepository.findByUserUsername(trainingDTO.getTraineeUsername())
				.orElseThrow(() -> new GymException("Trainee Not Found"));
		
		Trainer trainer = trainerRepository.findByUserUsername(trainingDTO.getTrainerUsername())
				.orElseThrow(() -> new GymException("Trainer Not Found"));

		trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee,trainingDTO.getTrainingDate()).ifPresent(t -> {
			throw new GymException(
					"Trainee is already associated with trainer. Training name :" + t.getTrainingName());
		});

		if (!trainer.getSpecialization().getTrainingTypeName().equalsIgnoreCase(trainingDTO.getTrainingType())) {
			throw new GymException("Trainer and training specialization didn't match");
		}

		if (!trainee.getTrainers().contains(trainer)) {
			throw new GymException("You haven't assigned to the trainer");
		}
		
		trainingRepository.save(trainingConverter.convertTrainingDTOtoTraining(trainingDTO, trainee, trainer));
		
		TrainingReportDTO trainingReportDTO = trainingConverter.getTrainingReportDTO(trainingDTO, trainer);
		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(trainingDTO, trainee, trainer);
		
		notificationProducer.sendNotification(notificationDTO);
		reportingProducer.sendReport(trainingReportDTO);
		
	}

	@Override
	public List<TrainingResponse> getTraineeTrainings(String username, Date periodFrom, Date periodTo,
			String trainerName, String trainingType) {

		log.info("Entered get trainee trainings method, username : {}",username);
		User user = userRepository.findByUsername(username).orElseThrow(() -> new GymException("Username not found"));
		return trainingRepository.getTraineeTrainings(user.getUsername(), periodFrom, periodTo, trainerName, trainingType);
	}

	@Override
	public List<TrainingResponse> getTrainerTrainings(String username, Date periodFrom, Date periodTo,
			String traineeName) {
		log.info("Entered get trainer trainings method, username : {}",username);
		User user = userRepository.findByUsername(username).orElseThrow(() -> new GymException("Username not found"));
		return trainingRepository.getTrainerTrainings(user.getUsername(), periodFrom, periodTo, traineeName);
	}

}
