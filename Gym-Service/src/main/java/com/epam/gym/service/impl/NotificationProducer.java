package com.epam.gym.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.epam.gym.dto.NotificationDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class NotificationProducer {
	
	private final KafkaTemplate<String,String> kafkaTemplate;
	private final ObjectMapper objectMapper;
	
	@Value("${notification.topic}")
	private String notificationTopic;
	
	public void sendNotification(NotificationDTO notificationDTO) throws JsonProcessingException {
		log.info("Entered send notification method, notificationDTO : {}",notificationDTO);
		String message = objectMapper.writeValueAsString(notificationDTO);
		try {
			kafkaTemplate.send(notificationTopic,message);
		}
		catch (Exception e) {
			log.error(String.format("Something went wrong : %s", e.getMessage()));
		}
	}
	
	
	@PreDestroy
	public void close() {
		if(kafkaTemplate!=null) {
			log.info("Kafka template is destroyed");
			kafkaTemplate.destroy();
		}
	}
	

}
