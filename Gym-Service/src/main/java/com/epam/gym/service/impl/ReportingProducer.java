package com.epam.gym.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.epam.gym.dto.TrainingReportDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class ReportingProducer {
	
	private final KafkaTemplate<String, String> kafkaTemplate;
	private final ObjectMapper objectMapper;
	
	@Value("${reporting.topic}")
	private String reportingTopic;
	
	public void sendReport(TrainingReportDTO trainingReportDTO) throws JsonProcessingException {
		log.info("Entered send Report method, TrainingReportDTO : {}",trainingReportDTO);
		String message = objectMapper.writeValueAsString(trainingReportDTO);
		try {
			kafkaTemplate.send(reportingTopic,message);
		}
		catch (Exception e) {
			log.error(String.format("Something went wrong : %s", e.getMessage()));
		}
	}
	
	@PreDestroy
	public void close() {
		if(kafkaTemplate!=null) {
			log.info("Kafka template is destroyed");
			kafkaTemplate.destroy();
		}
	}

}
