package com.epam.gym.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gym.dao.TraineeRepository;
import com.epam.gym.dao.TrainerRepository;
import com.epam.gym.dao.TrainingRepository;
import com.epam.gym.dao.UserRepository;
import com.epam.gym.dto.NotificationDTO;
import com.epam.gym.dto.TraineeDTO;
import com.epam.gym.dto.UpdateTraineeDTO;
import com.epam.gym.dto.response.RegistrationResponse;
import com.epam.gym.dto.response.TraineeProfileResponse;
import com.epam.gym.dto.response.TrainerResponse;
import com.epam.gym.dto.response.UpdatedTraineeResponse;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.Training;
import com.epam.gym.entity.User;
import com.epam.gym.exception.GymException;
import com.epam.gym.service.TraineeService;
import com.epam.gym.utility.CredentialGenerator;
import com.epam.gym.utility.NotificationConverter;
import com.epam.gym.utility.RegistrationConverter;
import com.epam.gym.utility.TraineeConverter;
import com.epam.gym.utility.TrainerConverter;
import com.epam.gym.utility.UserConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class TraineeServiceImpl implements TraineeService {

	private final UserRepository userRepository;
	private final TraineeRepository traineeRepository;
	private final TrainingRepository trainingRepository;
	private final TrainerRepository trainerRepository;
	private final NotificationProducer notificationProducer;
	private final CredentialGenerator generator;
	private final TraineeConverter traineeConverter;
	private final NotificationConverter notificationConverter;
	private final RegistrationConverter registrationConverter;
	private final UserConverter userConverter;
	private final PasswordEncoder encoder;
	private final TrainerConverter trainerConverter;

	private static final String TRAINEE_EXCEPTION_MESSAGE = "Trainee not found";

	@Override
	public RegistrationResponse traineeRegistration(TraineeDTO traineeDTO) throws JsonProcessingException {
		
		log.info("Entered trainee registration method, TraineeDTO : {}",traineeDTO);

		String name = traineeDTO.getFirstName().toLowerCase() + traineeDTO.getLastName().toLowerCase();
		List<String> usernames = userRepository.findByUsernameStartsWith(name).stream().map(User::getUsername).toList();
		String username = generator.generateUniqueUsername(traineeDTO.getFirstName(), traineeDTO.getLastName(),
				usernames);
		String password = generator.generatePassword();
		User user = userConverter.getUserFromTraineeDTO(traineeDTO, username,encoder.encode(password));

		Trainee trainee = traineeConverter.getTrainee(traineeDTO, user);

		traineeRepository.save(trainee);
		
		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(user,password);
		notificationProducer.sendNotification(notificationDTO);
		
		return registrationConverter.getRegistrationResponse(username, password);

	}

	@Override
	public TraineeProfileResponse getTraineeProfileDetails(String username) {
		
		log.info("Enteres get Trainee profile details , username : {}",username);

		Trainee trainee = traineeRepository.findByUserUsername(username)
				.orElseThrow(() -> new GymException(TRAINEE_EXCEPTION_MESSAGE));

		User user = trainee.getUser();

		List<TrainerResponse> trainerProfiles = trainee.getTrainers().stream().map(trainer -> {
			User tempUser = trainer.getUser();
			return trainerConverter.getTrainerResponse(trainer, tempUser);
		}).toList();

		return traineeConverter.getTraineeProfileResponse(trainee, user, trainerProfiles);

	}

	@Override
	public UpdatedTraineeResponse updateTraineeDetails(UpdateTraineeDTO updateTraineeDTO) throws JsonProcessingException {
		
		log.info("Entered update trainee details method, updateTraineeDTO : {}",updateTraineeDTO);

		Trainee trainee = traineeRepository.findByUserUsername(updateTraineeDTO.getUsername())
				.orElseThrow(() -> new GymException(TRAINEE_EXCEPTION_MESSAGE));

		trainee.setAddress(updateTraineeDTO.getAddress());
		trainee.setDateOfBirth(updateTraineeDTO.getDateOfBirth());

		User user = userConverter.getUserFromUpdateTraineeDTO(updateTraineeDTO, trainee.getUser());
		List<TrainerResponse> trainerProfiles = trainee.getTrainers().stream()
				.map(trainer -> trainerConverter.getTrainerResponse(trainer, trainer.getUser())).toList();
		
		
		UpdatedTraineeResponse updatedTraineeResponse = traineeConverter.getUpdatedTraineeResponse(trainee, user, trainerProfiles);
		
		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(trainee);
		notificationProducer.sendNotification(notificationDTO);
		
		return updatedTraineeResponse;

	}

	@Override
	public void deleteTrainee(String username) {
		
		log.info("Entered delete Trainee method, username : {}",username);

		Trainee trainee = traineeRepository.findByUserUsername(username)
				.orElseThrow(() -> new GymException(TRAINEE_EXCEPTION_MESSAGE));

		trainee.getTrainers().stream().forEach(trainer -> trainer.getTrainees().remove(trainee));

		traineeRepository.delete(trainee);
	}

	@Override
	public List<TrainerResponse> fetchNotAssignedTrainers(String traineeName) {
		
		log.info("Entered fetch not assigned trainers, traineeName : {}",traineeName);

		Trainee trainee = traineeRepository.findByUserUsername(traineeName)
				.orElseThrow(() -> new GymException(TRAINEE_EXCEPTION_MESSAGE));

		return trainerRepository.findByTraineesNotContaining(trainee).stream()
				.filter(trainer -> trainer.getUser().isActive())
				.map(trainer -> trainerConverter.getTrainerResponse(trainer, trainer.getUser())).toList();
	}

	@Override
	public List<TrainerResponse> updateTrainers(String traineeName, List<String> trainerNames) {
		
		log.info("Entered update trainers method, traineeName : {}, trainerNames : {}",traineeName,trainerNames);

		Trainee trainee = traineeRepository.findByUserUsername(traineeName)
				.orElseThrow(() -> new GymException(TRAINEE_EXCEPTION_MESSAGE));

		Set<Trainer> newTrainers = trainerRepository.findByUserUsernameIn(trainerNames);
		Set<Trainer> oldTrainers = trainee.getTrainers();
		oldTrainers.removeAll(newTrainers);

		oldTrainers.stream().forEach(trainer -> {
			trainer.getTrainees().remove(trainee);
			List<Training> trainings = trainingRepository.findByTrainerAndTrainee(trainer, trainee);
			trainingRepository.deleteAll(trainings);
		});

		trainee.setTrainers(newTrainers);

		return newTrainers.stream().map(trainer -> trainerConverter.getTrainerResponse(trainer, trainer.getUser())).toList();
				
	}

}
