package com.epam.gym.service;

import java.util.List;

import com.epam.gym.dto.TraineeDTO;
import com.epam.gym.dto.UpdateTraineeDTO;
import com.epam.gym.dto.response.RegistrationResponse;
import com.epam.gym.dto.response.TraineeProfileResponse;
import com.epam.gym.dto.response.TrainerResponse;
import com.epam.gym.dto.response.UpdatedTraineeResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface TraineeService {

	RegistrationResponse traineeRegistration(TraineeDTO traineeDTO) throws JsonProcessingException;
	
	TraineeProfileResponse getTraineeProfileDetails(String username);
	
	UpdatedTraineeResponse updateTraineeDetails(UpdateTraineeDTO updateTraineeDTO) throws JsonProcessingException;
	
	void deleteTrainee(String userName);
	
	List<TrainerResponse> fetchNotAssignedTrainers(String traineeName);
	
	List<TrainerResponse> updateTrainers(String traineeName,List<String> trainerNames);

}