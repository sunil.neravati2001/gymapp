package com.epam.gym.service.impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gym.dao.UserRepository;
import com.epam.gym.entity.User;
import com.epam.gym.exception.GymException;
import com.epam.gym.service.LoginService;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class LoginServiceImpl implements LoginService {

	private final UserRepository userRepository;
	private final PasswordEncoder encoder;

	@Override
	public void login(String userName, String password) {
		
		log.info("Entered login method, username : {},password : {}",userName,password);
		
		User user = userRepository.findByUsername(userName).orElseThrow(() -> new GymException("User not Found"));
		
		if (!encoder.matches(password, user.getPassword())) {
			throw new GymException("Password is not correct");
		}
		
	}

	@Override
	@Transactional
	public void changePassword(String userName, String oldPassword, String newpassWord) {
		
		log.info("Entered change password method, username : {}, oldpassword : {}, newpassword : {}",userName,oldPassword,newpassWord);
		User user=userRepository.findByUsername(userName)
				.orElseThrow(() -> new GymException("Invalid user details"));
		
		if (!encoder.matches(oldPassword, user.getPassword())) {
			throw new GymException("Password is not correct");
		}else {
			user.setPassword(encoder.encode(newpassWord));
		}
		
	}

}
