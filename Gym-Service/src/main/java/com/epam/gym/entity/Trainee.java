package com.epam.gym.entity;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Trainee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private Date dateOfBirth;

	private String address;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", unique = true, nullable = false)
	private User user;

	@ManyToMany(mappedBy = "trainees")
	@JsonIgnoreProperties("trainees")
	private Set<Trainer> trainers;

	@OneToMany(mappedBy = "trainee", cascade = CascadeType.REMOVE)
	private Set<Training> trainings;

	public void setTrainers(Set<Trainer> trainers) {
		this.trainers.clear();
		if (trainers != null) {
			this.trainers.addAll(trainers);
			trainers.forEach(trainer -> trainer.getTrainees().add(this));
		}
	}
}
