package com.epam.gym.utility;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.epam.gym.dto.TraineeDTO;
import com.epam.gym.dto.TrainerDTO;
import com.epam.gym.dto.UpdateTraineeDTO;
import com.epam.gym.dto.UpdateTrainerDTO;
import com.epam.gym.entity.User;

@Component
public class UserConverter {
	
	private UserConverter() {}
	
	public User getUserFromUpdateTraineeDTO(UpdateTraineeDTO updateTraineeDTO, User user) {
		user.setEmail(updateTraineeDTO.getEmail());
		user.setActive(updateTraineeDTO.getIsActive());
		user.setFirstName(updateTraineeDTO.getFirstName());
		user.setLastName(updateTraineeDTO.getLastName());
		return user;
	}

	public User getUserFromUpdateTrainerDTO(UpdateTrainerDTO updateTrainerDTO, User user) {
		user.setActive(updateTrainerDTO.getIsActive());
		user.setFirstName(updateTrainerDTO.getFirstName());
		user.setLastName(updateTrainerDTO.getLastName());
		user.setEmail(updateTrainerDTO.getEmail());
		return user;
	}
	
	public User getUserFromTraineeDTO(TraineeDTO traineeDTO, String username, String password) {
		return User.builder().firstName(traineeDTO.getFirstName()).lastName(traineeDTO.getLastName()).username(username)
				.password(password).email(traineeDTO.getEmail()).createdDate(new Date())
				.isActive(true).build();
	}

	public User getUserFromTrainerDTO(TrainerDTO trainerDTO, String username, String password) {
		return User.builder().firstName(trainerDTO.getFirstName()).lastName(trainerDTO.getLastName()).username(username)
				.password(password).email(trainerDTO.getEmail()).createdDate(new Date())
				.isActive(true).build();
	}

}
