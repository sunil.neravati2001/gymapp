package com.epam.gym.utility;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.epam.gym.dto.NotificationDTO;
import com.epam.gym.dto.TrainingDTO;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.User;

@Component
public class NotificationConverter {
	
	private NotificationConverter() {}
	
	public NotificationDTO getNotificationDTO(Trainee trainee) {
		User user = trainee.getUser();
		Map<String,String> body = new LinkedHashMap<>();
		body.put("trainee first name", user.getFirstName());
		body.put("trainee last name", user.getLastName());
		body.put("DOB", trainee.getDateOfBirth().toString());
		body.put("email", user.getEmail());
		body.put("Address", trainee.getAddress());
		body.put("Status", user.isActive()? "Active" : "InActive");
		
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO.setToEmails(List.of(user.getEmail()));
		notificationDTO.setSubject("UPDATE_TRAINEE");
		notificationDTO.setBody(body);
		return notificationDTO;
	}
	
	public NotificationDTO getNotificationDTO(User user, String password) {
		Map<String,String> body = new LinkedHashMap<>();
		body.put("username", user.getUsername());
		body.put("password", password);
		
		NotificationDTO notificationDTO =  new NotificationDTO();
		notificationDTO.setToEmails(List.of(user.getEmail()));
		notificationDTO.setSubject("REGISTRATION");
		notificationDTO.setBody(body);
		return notificationDTO;
	}
	
	public NotificationDTO getNotificationDTO(Trainer trainer) {
		User user = trainer.getUser();
		Map<String,String> body = new LinkedHashMap<>();
		body.put("trainer first name", user.getFirstName());
		body.put("trainer last name", user.getLastName());
		body.put("email", user.getEmail());
		body.put("Status", user.isActive()? "Active" : "InActive");
		
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO.setToEmails(List.of(user.getEmail()));
		notificationDTO.setSubject("UPDATE_TRAINER");
		notificationDTO.setBody(body);
		return notificationDTO;
	}
	
	public NotificationDTO getNotificationDTO(TrainingDTO trainingDTO, Trainee trainee, Trainer trainer) {
		Map<String,String> body = new LinkedHashMap<>();
		body.put("training", trainingDTO.getTrainingName());
		body.put("trainee first name", trainee.getUser().getFirstName());
		body.put("trainer last name", trainer.getUser().getFirstName());
		
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO.setToEmails(List.of(trainee.getUser().getEmail(),trainer.getUser().getEmail()));
		notificationDTO.setSubject("TRAINING_REGISTRATION");
		notificationDTO.setBody(body);
		return notificationDTO;
	}
  
  
}
