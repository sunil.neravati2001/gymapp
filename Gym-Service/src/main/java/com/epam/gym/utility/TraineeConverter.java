package com.epam.gym.utility;

import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.gym.dto.TraineeDTO;
import com.epam.gym.dto.response.TraineeProfileResponse;
import com.epam.gym.dto.response.TraineeResponse;
import com.epam.gym.dto.response.TrainerResponse;
import com.epam.gym.dto.response.UpdatedTraineeResponse;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.User;

@Component
public class TraineeConverter {

	private TraineeConverter() {
	}

	public TraineeResponse getTraineeResponse(User tempUser) {
		TraineeResponse traineeResponse = new TraineeResponse();
		traineeResponse.setFirstName(tempUser.getFirstName());
		traineeResponse.setLastName(tempUser.getLastName());
		traineeResponse.setUsername(tempUser.getUsername());
		return traineeResponse;
	}

	public TraineeProfileResponse getTraineeProfileResponse(Trainee trainee, User user,
			List<TrainerResponse> trainerProfiles) {
		TraineeProfileResponse traineeProfileResponse = new TraineeProfileResponse();
		traineeProfileResponse.setFirstName(user.getFirstName());
		traineeProfileResponse.setLastName(user.getLastName());
		traineeProfileResponse.setDateOfBirth(trainee.getDateOfBirth());
		traineeProfileResponse.setAddress(trainee.getAddress());
		traineeProfileResponse.setActive(user.isActive());
		traineeProfileResponse.setTrainers(trainerProfiles);
		return traineeProfileResponse;
	}


	public UpdatedTraineeResponse getUpdatedTraineeResponse(Trainee trainee, User user,
			List<TrainerResponse> trainerProfiles) {
		UpdatedTraineeResponse traineeProfileResponse = new UpdatedTraineeResponse();
		traineeProfileResponse.setFirstName(user.getFirstName());
		traineeProfileResponse.setLastName(user.getLastName());
		traineeProfileResponse.setUsername(user.getUsername());
		traineeProfileResponse.setEmail(user.getEmail());
		traineeProfileResponse.setDateOfBirth(trainee.getDateOfBirth());
		traineeProfileResponse.setAddress(trainee.getAddress());
		traineeProfileResponse.setActive(user.isActive());
		traineeProfileResponse.setTrainers(trainerProfiles);
		return traineeProfileResponse;
	}
	
	public Trainee getTrainee(TraineeDTO traineeDTO, User user) {
		Trainee trainee = new Trainee();
		trainee.setUser(user);
		trainee.setDateOfBirth(traineeDTO.getDateOfBirth());
		trainee.setAddress(traineeDTO.getAddress());
		return trainee;
	}

}
