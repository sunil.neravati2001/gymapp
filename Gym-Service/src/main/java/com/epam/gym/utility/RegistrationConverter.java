package com.epam.gym.utility;

import org.springframework.stereotype.Component;

import com.epam.gym.dto.response.RegistrationResponse;

@Component
public class RegistrationConverter {
	
	private RegistrationConverter() {}
	
	public RegistrationResponse getRegistrationResponse(String username, String password) {
		RegistrationResponse registrationResponse = new RegistrationResponse();
		registrationResponse.setUsername(username);
		registrationResponse.setPassword(password);
		return registrationResponse;
	}

}
