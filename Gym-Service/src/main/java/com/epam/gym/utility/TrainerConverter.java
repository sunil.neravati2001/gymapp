package com.epam.gym.utility;

import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.gym.dto.response.TraineeResponse;
import com.epam.gym.dto.response.TrainerProfileResponse;
import com.epam.gym.dto.response.TrainerResponse;
import com.epam.gym.dto.response.UpdatedTrainerResponse;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.User;

@Component
public class TrainerConverter {
	
	private TrainerConverter() {}
	
	public UpdatedTrainerResponse getUpdatedResponse(Trainer trainer, User user,
			List<TraineeResponse> traineeProfiles) {
		UpdatedTrainerResponse trainerProfileResponse = new UpdatedTrainerResponse();
		trainerProfileResponse.setFirstName(user.getFirstName());
		trainerProfileResponse.setLastName(user.getLastName());
		trainerProfileResponse.setUsername(user.getUsername());
		trainerProfileResponse.setEmail(user.getEmail());
		trainerProfileResponse.setSpecialization(trainer.getSpecialization().getTrainingTypeName());
		trainerProfileResponse.setIsActive(user.isActive());
		trainerProfileResponse.setTrainees(traineeProfiles);
		return trainerProfileResponse;
	}
	
	public TrainerProfileResponse getTrainerProfileResponse(Trainer trainer, User user,
			List<TraineeResponse> traineeProfiles) {
		TrainerProfileResponse trainerProfileResponse = new TrainerProfileResponse();
		trainerProfileResponse.setFirstName(user.getFirstName());
		trainerProfileResponse.setLastName(user.getLastName());
		trainerProfileResponse.setSpecialization(trainer.getSpecialization().getTrainingTypeName());
		trainerProfileResponse.setIsActive(user.isActive());
		trainerProfileResponse.setTrainees(traineeProfiles);
		return trainerProfileResponse;
	}
	
	public TrainerResponse getTrainerResponse(Trainer trainer, User tempUser) {
		TrainerResponse trainerResponse = new TrainerResponse();
		trainerResponse.setSpecialization(trainer.getSpecialization().getTrainingTypeName());
		trainerResponse.setFirstName(tempUser.getFirstName());
		trainerResponse.setLastName(tempUser.getLastName());
		trainerResponse.setUsername(tempUser.getUsername());
		return trainerResponse;
	}

}
