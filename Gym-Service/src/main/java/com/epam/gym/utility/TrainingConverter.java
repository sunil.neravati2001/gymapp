package com.epam.gym.utility;

import org.springframework.stereotype.Component;

import com.epam.gym.dto.TrainingDTO;
import com.epam.gym.dto.TrainingReportDTO;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.Training;
import com.epam.gym.entity.User;

@Component
public class TrainingConverter {
	
	private TrainingConverter() {}
	
	public Training convertTrainingDTOtoTraining(TrainingDTO trainingDTO, Trainee trainee, Trainer trainer) {
		Training training = new Training();
		training.setTrainingDate(trainingDTO.getTrainingDate());
		training.setTrainingDuration(trainingDTO.getDuration());
		training.setTrainingType(trainer.getSpecialization());
		training.setTrainingName(trainingDTO.getTrainingName());
		training.setTrainee(trainee);
		training.setTrainer(trainer);
		return training;
	}

	public TrainingReportDTO getTrainingReportDTO(TrainingDTO trainingDTO, Trainer trainer) {
		TrainingReportDTO trainingReportDTO = new TrainingReportDTO();
		User user = trainer.getUser();
		trainingReportDTO.setFirstName(user.getFirstName());
		trainingReportDTO.setLastName(user.getLastName());
		trainingReportDTO.setUsername(user.getUsername());
		trainingReportDTO.setIsActive(user.isActive());
		trainingReportDTO.setTrainingDate(trainingDTO.getTrainingDate());
		trainingReportDTO.setTrainingDuration(trainingDTO.getDuration());
		trainingReportDTO.setTrainingName(trainingDTO.getTrainingName());
		return trainingReportDTO;
	}
	
}
