package com.epam.gym.exceptionhandler;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.gym.dto.response.ExceptionResponse;
import com.epam.gym.exception.GymException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ExceptionResponse> argumentValidationException(MethodArgumentNotValidException errors,
			WebRequest request) {
		Map<String, String> result = new HashMap<>();
		errors.getBindingResult().getFieldErrors().forEach(error -> result.put(error.getField(), error.getDefaultMessage()));
		String message = result.toString();
		log.error(message);
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), message,
				request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ExceptionResponse> runtimeExceptionHandler(RuntimeException message, WebRequest request) {
		log.error("Something went wrong " + message.getMessage());
		return new ResponseEntity<>(
				new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.name(),
						"Something went wrong " + message.getMessage(), request.getDescription(false)),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(GymException.class)
	public ResponseEntity<ExceptionResponse> gymExceptionalHandler(GymException message, WebRequest request) {
		log.error(message.getMessage());
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				message.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}
}
