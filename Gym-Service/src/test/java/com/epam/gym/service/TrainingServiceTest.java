package com.epam.gym.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.gym.dao.TraineeRepository;
import com.epam.gym.dao.TrainerRepository;
import com.epam.gym.dao.TrainingRepository;
import com.epam.gym.dao.UserRepository;
import com.epam.gym.dto.NotificationDTO;
import com.epam.gym.dto.TrainingDTO;
import com.epam.gym.dto.TrainingReportDTO;
import com.epam.gym.dto.response.TrainingResponse;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.Training;
import com.epam.gym.entity.TrainingType;
import com.epam.gym.entity.User;
import com.epam.gym.exception.GymException;
import com.epam.gym.service.impl.NotificationProducer;
import com.epam.gym.service.impl.ReportingProducer;
import com.epam.gym.service.impl.TrainingServiceImpl;
import com.epam.gym.utility.NotificationConverter;
import com.epam.gym.utility.TrainingConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(MockitoExtension.class)
class TrainingServiceTest {

	private TrainingDTO trainingDTO;

	private Trainee trainee;

	private Trainer trainer;

	private TrainingType trainingType;

	private User traineeUser;

	private User trainerUser;

	@Mock
	private TrainingRepository trainingRepository;

	@Mock
	private TrainerRepository trainerRepository;

	@Mock
	private TraineeRepository traineeRepository;

	@Mock
	private UserRepository userRepository;

	@Mock
	private NotificationProducer notificationProducer;

	@Mock
	private ReportingProducer reportingProducer;

	@Mock
	private NotificationConverter notificationConverter;

	@Mock
	private TrainingConverter trainingConverter;

	@InjectMocks
	private TrainingServiceImpl trainingServiceImpl;

	@Test
	void testAddTrainingSuccessful() throws JsonProcessingException {

		Set<Trainer> trainers = new HashSet<>();
		trainers.add(trainer);
		trainee.getTrainers().add(trainer);

		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(trainerRepository.findByUserUsername("3412")).thenReturn(Optional.of(trainer));
		Mockito.when(trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee,
				trainingDTO.getTrainingDate())).thenReturn(Optional.empty());

		Mockito.when(trainingConverter.convertTrainingDTOtoTraining(any(), any(), any())).thenReturn(new Training());
		Mockito.when(trainingConverter.getTrainingReportDTO(any(), any())).thenReturn(new TrainingReportDTO());
		Mockito.when(notificationConverter.getNotificationDTO(any(), any(), any())).thenReturn(new NotificationDTO());

		trainingServiceImpl.addTraining(trainingDTO);

		Mockito.verify(trainingRepository, times(1)).save(any());
		Mockito.verify(reportingProducer, times(1)).sendReport(any());
		Mockito.verify(notificationProducer, times(1)).sendNotification(any(NotificationDTO.class));
	}

	@Test
	void testAddTrainingTraineeNotFound() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> trainingServiceImpl.addTraining(trainingDTO));
	}

	@Test
	void testAddTrainingTrainerNotFound() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(trainerRepository.findByUserUsername("3412")).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> trainingServiceImpl.addTraining(trainingDTO));
	}

	@Test
	void testAddTrainingTraineeAlreadyAssigned() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(trainerRepository.findByUserUsername("3412")).thenReturn(Optional.of(trainer));
		Mockito.when(trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee,
				trainingDTO.getTrainingDate())).thenReturn(Optional.of(new Training()));

		assertThrows(GymException.class, () -> trainingServiceImpl.addTraining(trainingDTO));
	}

	@Test
	void testAddTrainingTrainerSpecializationMismatch() {
		trainingDTO.setTrainingType("invalidType");
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(trainerRepository.findByUserUsername("3412")).thenReturn(Optional.of(trainer));
		Mockito.when(trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee,
				trainingDTO.getTrainingDate())).thenReturn(Optional.empty());

		assertThrows(GymException.class, () -> trainingServiceImpl.addTraining(trainingDTO));
	}

	@Test
	void testAddTrainingUnassignedTrainee() {

		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(trainerRepository.findByUserUsername("3412")).thenReturn(Optional.of(trainer));
		Mockito.when(trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee,
				trainingDTO.getTrainingDate())).thenReturn(Optional.empty());

		assertThrows(GymException.class, () -> trainingServiceImpl.addTraining(trainingDTO));
	}

	@Test
	void testGetTraineeTrainings() {

		Mockito.when(userRepository.findByUsername("1234")).thenReturn(Optional.of(traineeUser));
		Mockito.when(trainingRepository.getTraineeTrainings(any(), any(), any(), any(), any()))
				.thenReturn(List.of(new TrainingResponse()));

		trainingServiceImpl.getTraineeTrainings("1234", new Date(), new Date(), "3412", "zumba");

		Mockito.verify(userRepository, times(1)).findByUsername("1234");
		Mockito.verify(trainingRepository, times(1)).getTraineeTrainings(any(), any(), any(), any(), any());
	}

	@Test
	void testGetTraineeTrainingsWithInvalidTrainer() {

		Date periodFrom = new Date();
		Date periodTo = new Date();

		Mockito.when(userRepository.findByUsername("1234")).thenReturn(Optional.empty());

		assertThrows(GymException.class,
				() -> trainingServiceImpl.getTraineeTrainings("1234", periodFrom, periodTo, "3412", "zumba"));

		Mockito.verify(userRepository, times(1)).findByUsername("1234");

	}

	@Test
	void testGetTrainerTrainings() {

		Mockito.when(userRepository.findByUsername("3412")).thenReturn(Optional.of(trainerUser));
		Mockito.when(trainingRepository.getTrainerTrainings(any(), any(), any(), any()))
				.thenReturn(List.of(new TrainingResponse()));

		trainingServiceImpl.getTrainerTrainings("3412", new Date(), new Date(), "1234");

		Mockito.verify(userRepository, times(1)).findByUsername("3412");
		Mockito.verify(trainingRepository, times(1)).getTrainerTrainings(any(), any(), any(), any());
	}

	@Test
	void testGetTrainerTrainingsWithInvalidTrainer() {

		Date periodFrom = new Date();
		Date periodTo = new Date();

		Mockito.when(userRepository.findByUsername("3412")).thenReturn(Optional.empty());

		assertThrows(GymException.class,
				() -> trainingServiceImpl.getTrainerTrainings("3412", periodFrom, periodTo, "1234"));

		Mockito.verify(userRepository, times(1)).findByUsername("3412");
	}

	@BeforeEach
	void setUpTraineeUser() {
		traineeUser = User.builder().createdDate(new Date()).email("123@gmail.com").firstName("12").lastName("34")
				.username("1234").password("1234").isActive(true).id(1).build();
	}

	@BeforeEach
	void setUpTrainerUser() {
		trainerUser = User.builder().createdDate(new Date()).email("1234@gmail.com").firstName("34").lastName("12")
				.username("3421").password("1234").isActive(true).id(2).build();
	}

	@BeforeEach
	void setUpTrainee() {
		trainee = Trainee.builder().id(1).address("hyd").user(traineeUser).trainers(new HashSet<>()).build();
	}

	@BeforeEach
	void setUpTrainer() {
		trainer = new Trainer();
		trainer.setUser(trainerUser);
		trainer.setSpecialization(trainingType);
	}

	@BeforeEach
	void setUpTrainingType() {
		trainingType = new TrainingType();
		trainingType.setId(1);
		trainingType.setTrainingTypeName("zumba");
	}

	@BeforeEach
	void setUpTrainingDTO() {
		trainingDTO = new TrainingDTO();
		trainingDTO.setTraineeUsername("1234");
		trainingDTO.setTrainerUsername("3412");
		trainingDTO.setTrainingType("zumba");
		trainingDTO.setDuration(3);
		trainingDTO.setTrainingName("zumba-training");
		trainingDTO.setTrainingDate(new Date());
	}

}
