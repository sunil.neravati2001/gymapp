package com.epam.gym.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.gym.dao.UserRepository;
import com.epam.gym.entity.User;
import com.epam.gym.exception.GymException;
import com.epam.gym.service.impl.LoginServiceImpl;

@ExtendWith(MockitoExtension.class)
class LoginServiceTest {

	private User user;

	@Mock
	private UserRepository userRepository;

	@Mock
	private PasswordEncoder encoder;

	@InjectMocks
	private LoginServiceImpl loginService;

	@Test
	void login() {

		Mockito.when(userRepository.findByUsername(any())).thenReturn(Optional.of(user));
		Mockito.when(encoder.matches(any(), any())).thenReturn(true);

		loginService.login("1234", "1234");

		Mockito.verify(userRepository).findByUsername(any());
		Mockito.verify(encoder).matches(any(), any());
	}

	@Test
	void loginWithException() {

		assertThrows(GymException.class, () -> loginService.login("11", "1"));

	}

	@Test
	void loginWithIncorrectPassword() {

		Mockito.when(userRepository.findByUsername(any())).thenReturn(Optional.of(user));
		Mockito.when(encoder.matches(any(), any())).thenReturn(false);

		assertThrows(GymException.class, () -> loginService.login("11", "91"));

		Mockito.verify(userRepository).findByUsername(any());
		Mockito.verify(encoder).matches(any(), any());
	}

	@Test
	void changePassword() {
		Mockito.when(userRepository.findByUsername(any())).thenReturn(Optional.of(user));
		Mockito.when(encoder.matches(any(), any())).thenReturn(true);
		Mockito.when(encoder.encode(any())).thenReturn("123456");

		loginService.changePassword("1234", "1234", "123456");

		Mockito.verify(userRepository).findByUsername(any());
		Mockito.verify(encoder).matches(any(), any());
		Mockito.verify(encoder).encode(any());
	}

	@Test
	void changePasswordWithException() {
		assertThrows(GymException.class, () -> loginService.changePassword("11", "1", "88"));
	}

	@Test
	void changePasswordWithIncorrectOldPassword() {

		Mockito.when(userRepository.findByUsername(any())).thenReturn(Optional.of(user));
		Mockito.when(encoder.matches(any(), any())).thenReturn(false);

		assertThrows(GymException.class, () -> loginService.changePassword("11", "1", "88"));

		Mockito.verify(userRepository).findByUsername(any());
		Mockito.verify(encoder).matches(any(), any());
	}

	@BeforeEach
	void setUpUser() {
		user = User.builder().email("123@gmail.com").firstName("12").lastName("34").username("1234").password("1234")
				.isActive(true).createdDate(new Date()).build();
	}

}