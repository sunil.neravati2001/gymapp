package com.epam.gym.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.gym.dao.TraineeRepository;
import com.epam.gym.dao.TrainerRepository;
import com.epam.gym.dao.TrainingRepository;
import com.epam.gym.dao.TrainingTypeRepository;
import com.epam.gym.dao.UserRepository;
import com.epam.gym.dto.NotificationDTO;
import com.epam.gym.dto.TraineeDTO;
import com.epam.gym.dto.TrainerDTO;
import com.epam.gym.dto.UpdateTraineeDTO;
import com.epam.gym.dto.UpdateTrainerDTO;
import com.epam.gym.dto.response.RegistrationResponse;
import com.epam.gym.dto.response.TraineeProfileResponse;
import com.epam.gym.dto.response.TraineeResponse;
import com.epam.gym.dto.response.TrainerProfileResponse;
import com.epam.gym.dto.response.TrainerResponse;
import com.epam.gym.dto.response.UpdatedTraineeResponse;
import com.epam.gym.dto.response.UpdatedTrainerResponse;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.TrainingType;
import com.epam.gym.entity.User;
import com.epam.gym.exception.GymException;
import com.epam.gym.service.impl.NotificationProducer;
import com.epam.gym.service.impl.TrainerServiceImpl;
import com.epam.gym.utility.CredentialGenerator;
import com.epam.gym.utility.NotificationConverter;
import com.epam.gym.utility.RegistrationConverter;
import com.epam.gym.utility.TraineeConverter;
import com.epam.gym.utility.TrainerConverter;
import com.epam.gym.utility.UserConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(MockitoExtension.class)
class TrainerServiceTest {
	
	private Trainee trainee;
	
	private User traineeUser;
	
	private User trainerUser;
	
	private TraineeDTO traineeDTO;
	
	private TrainerDTO trainerDTO;
	
	private NotificationDTO notificationDTO;
	
	private RegistrationResponse registrationResponse;
	
	private TraineeProfileResponse traineeProfileResponse;
	
	private TrainerProfileResponse trainerProfileResponse;
	
	private UpdateTraineeDTO updateTraineeDTO;
	
	private UpdateTrainerDTO updateTrainerDTO;
	
	private UpdatedTraineeResponse updatedTraineeResponse;
	
	private UpdatedTrainerResponse updatedTrainerResponse;
	
	private Trainer trainer;
	
	private Trainer trainerWithTrainee;
	
	private TrainingType trainingType;
	
	private TrainerResponse trainerResponse;
	
	private TraineeResponse traineeResponse;
	
	@Mock
	private UserRepository userRepository;
	
	@Mock
	private TraineeRepository traineeRepository;
	
	@Mock
	private TrainingRepository trainingRepository;
	
	@Mock
	private TrainerRepository trainerRepository;
	
	@Mock
	private NotificationProducer notificationProducer;
	
	@Mock
	private PasswordEncoder encoder;
	
	@Mock
	private CredentialGenerator generator;
	
	@Mock
	private NotificationConverter notificationConverter;
	
	@Mock
	private RegistrationConverter registrationConverter;
	
	@Mock
	private UserConverter userConverter;
	
	@Mock
	private TraineeConverter traineeConverter;
	
	@Mock
	private TrainerConverter trainerConverter;
	
	@Mock
	private TrainingTypeRepository trainingTypeRepository;
	
	@InjectMocks
	private TrainerServiceImpl trainerServiceImpl;
	
	@Test
	void testTrainerRegistration() throws JsonProcessingException {
		
        Mockito.when(userRepository.findByUsernameStartsWith(any())).thenReturn(List.of());
        Mockito.when(generator.generateUniqueUsername(any(), any(), any())).thenReturn("1234");
        Mockito.when(generator.generatePassword()).thenReturn("1234");
        Mockito.when(encoder.encode("1234")).thenReturn("1234");
        Mockito.when(userConverter.getUserFromTrainerDTO(any(), any(), any())).thenReturn(trainerUser);
        Mockito.when(trainingTypeRepository.findByTrainingTypeName("zumba")).thenReturn(Optional.of(trainingType));
        Mockito.when(notificationConverter.getNotificationDTO(any(), any())).thenReturn(notificationDTO);
        Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);
        Mockito.when(registrationConverter.getRegistrationResponse(any(), any())).thenReturn(registrationResponse);

        assertEquals(registrationResponse, trainerServiceImpl.trainerRegistration(trainerDTO));

        Mockito.verify(userRepository).findByUsernameStartsWith(any());
        Mockito.verify(generator).generateUniqueUsername(any(), any(), any());
        Mockito.verify(generator).generatePassword();
        Mockito.verify(encoder).encode("1234");
        Mockito.verify(userConverter).getUserFromTrainerDTO(any(), any(), any());
        Mockito.verify(trainingTypeRepository).findByTrainingTypeName("zumba");
        Mockito.verify(notificationConverter).getNotificationDTO(any(), any());
        Mockito.verify(registrationConverter).getRegistrationResponse(any(), any());
	}
	
	@Test
	void testTrainerRegistrationWithInvalidTrainingType() throws JsonProcessingException {
		
        Mockito.when(userRepository.findByUsernameStartsWith(any())).thenReturn(List.of());
        Mockito.when(generator.generateUniqueUsername(any(), any(), any())).thenReturn("1234");
        Mockito.when(generator.generatePassword()).thenReturn("1234");
        Mockito.when(encoder.encode("1234")).thenReturn("1234");
        Mockito.when(userConverter.getUserFromTrainerDTO(any(), any(), any())).thenReturn(trainerUser);
        Mockito.when(trainingTypeRepository.findByTrainingTypeName(any())).thenReturn(Optional.empty());
        Mockito.when(notificationConverter.getNotificationDTO(any(), any())).thenReturn(notificationDTO);
        Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);
        Mockito.when(registrationConverter.getRegistrationResponse(any(), any())).thenReturn(registrationResponse);

        assertEquals(registrationResponse, trainerServiceImpl.trainerRegistration(trainerDTO));

        Mockito.verify(userRepository).findByUsernameStartsWith(any());
        Mockito.verify(generator).generateUniqueUsername(any(), any(), any());
        Mockito.verify(generator).generatePassword();
        Mockito.verify(encoder).encode("1234");
        Mockito.verify(userConverter).getUserFromTrainerDTO(any(), any(), any());
        Mockito.verify(trainingTypeRepository).findByTrainingTypeName(any());
        Mockito.verify(notificationConverter).getNotificationDTO(any(), any());
        Mockito.verify(registrationConverter).getRegistrationResponse(any(), any());
	}
	
	@Test
	void testGetTrainerProfileResponse() {
		Mockito.when(trainerRepository.findByUserUsername(any())).thenReturn(Optional.of(trainerWithTrainee));
		Mockito.when(traineeConverter.getTraineeResponse(any())).thenReturn(traineeResponse);
		Mockito.when(trainerConverter.getTrainerProfileResponse(any(), any(), any())).thenReturn(trainerProfileResponse);
		assertEquals(trainerProfileResponse, trainerServiceImpl.getTrainerProfileDetails("3412"));
		Mockito.verify(trainerRepository).findByUserUsername(any());
		Mockito.verify(traineeConverter).getTraineeResponse(any());
		Mockito.verify(trainerConverter).getTrainerProfileResponse(any(), any(), any());
	}
	
	@Test
	void testGetInvalidTrainerProfile() {
		Mockito.when(trainerRepository.findByUserUsername(any())).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> trainerServiceImpl.getTrainerProfileDetails("3412"));
		Mockito.verify(trainerRepository).findByUserUsername(any());
	}
	
	@Test
	void testUpdateTrainerDetails() throws JsonProcessingException {
		Mockito.when(trainerRepository.findByUserUsername(any())).thenReturn(Optional.of(trainerWithTrainee));
		Mockito.when(userConverter.getUserFromUpdateTrainerDTO(any(), any())).thenReturn(trainerUser);
		Mockito.when(traineeConverter.getTraineeResponse(any())).thenReturn(traineeResponse);
		Mockito.when(trainerConverter.getUpdatedResponse(any(), any(), any())).thenReturn(updatedTrainerResponse);
		Mockito.when(notificationConverter.getNotificationDTO(trainerWithTrainee)).thenReturn(notificationDTO);
		Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);
		assertEquals(updatedTrainerResponse, trainerServiceImpl.updateTrainerDetails(updateTrainerDTO));
		Mockito.verify(trainerRepository).findByUserUsername(any());
		Mockito.verify(userConverter).getUserFromUpdateTrainerDTO(any(), any());
		Mockito.verify(traineeConverter).getTraineeResponse(any());
		Mockito.verify(trainerConverter).getUpdatedResponse(any(), any(), any());
		Mockito.verify(notificationConverter).getNotificationDTO(trainerWithTrainee);
	}
	
	@Test
	void testUpdatedInvalidTrainerProfile() {
		Mockito.when(trainerRepository.findByUserUsername(any())).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> trainerServiceImpl.updateTrainerDetails(updateTrainerDTO));
		Mockito.verify(trainerRepository).findByUserUsername(any());
	}
	
	@BeforeEach
	void setUpUpdatedTrainerDTO() {
		updateTrainerDTO = new UpdateTrainerDTO();
		updateTrainerDTO.setEmail("1234@gmail.com");
		updateTrainerDTO.setFirstName("34");
		updateTrainerDTO.setIsActive(true);
		updateTrainerDTO.setLastName("12");
		updateTrainerDTO.setSpecialization("zumba");
		updateTrainerDTO.setUsername("3412");;
	}
	
	@BeforeEach
	void setUpUpdatedTrainerResponse() {
		updatedTrainerResponse = new UpdatedTrainerResponse();
		updatedTrainerResponse.setEmail("1234@gmail.com");
		updatedTrainerResponse.setFirstName("34");
		updatedTrainerResponse.setIsActive(true);
		updatedTrainerResponse.setLastName("12");
		updatedTrainerResponse.setUsername("3412");
		updatedTrainerResponse.setSpecialization("zumba");;
	}
	
	@BeforeEach
	void setUpTrainerProfileResponsex() {
		trainerProfileResponse = new TrainerProfileResponse();
		trainerProfileResponse.setFirstName("34");
		trainerProfileResponse.setLastName("12");
		trainerProfileResponse.setSpecialization("zumba");
		trainerProfileResponse.setIsActive(true);		
	}
	
	@BeforeEach
	void setUpTraineeResponse() {
		traineeResponse = new TraineeResponse();
		traineeResponse.setFirstName("12");
		traineeResponse.setLastName("34");
		traineeResponse.setUsername("1234");;
	}
	
	@BeforeEach
	void setUpTrainerDTO() {
		trainerDTO = new TrainerDTO();
		trainerDTO.setEmail("1234@gmail.com");
		trainerDTO.setFirstName("34");
		trainerDTO.setLastName("12");
		trainerDTO.setSpecialization("zumba");;
	}

	@BeforeEach
	void setUpTrainingType() {
		trainingType = new TrainingType();
		trainingType.setId(1);
		trainingType.setTrainingTypeName("zumba");
	}
	
	@BeforeEach
	void setUpTrainee() {
	    trainee = Trainee.builder().id(1).address("hyd").user(traineeUser).trainers(new HashSet<>()).build();
	}
	
	@BeforeEach
	void setUpTrainer() {
		trainer = new Trainer();
		trainer.setUser(trainerUser);
		trainer.setSpecialization(trainingType);
	}
	
	@BeforeEach
	void setUpTainerWithTrainee() {
		trainerWithTrainee = new Trainer();
		trainerWithTrainee.setSpecialization(trainingType);
		trainerWithTrainee.setUser(trainerUser);
		trainerWithTrainee.setTrainees(Set.of(trainee));;
	}
	
	@BeforeEach
	void setUpTraineeUser() {
		traineeUser = User.builder().createdDate(new Date()).email("123@gmail.com").firstName("12").lastName("34").username("1234").password("1234").isActive(true).id(1).build();
	}
	
	@BeforeEach
	void setUpTrainerUser() {
		trainerUser = User.builder().createdDate(new Date()).email("1234@gmail.com").firstName("34").lastName("12").username("3421").password("1234").isActive(true).id(2).build();
	}
	
	@BeforeEach
	void setUpTraineeDTO() {
		traineeDTO = new TraineeDTO();
		traineeDTO.setAddress("hyd");
		traineeDTO.setEmail("123@gmail.com");
		traineeDTO.setFirstName("12");
		traineeDTO.setLastName("34");
	}
	
	@BeforeEach
	void setUpNotificationDTO() {
		
		Map<String,String> body = new LinkedHashMap<>();
		body.put("username", "1234");
		body.put("password", "1234");
		
		NotificationDTO notificationDTO =  new NotificationDTO();
		notificationDTO.setToEmails(List.of("123@gmail.com"));
		notificationDTO.setSubject("REGISTRATION");
		notificationDTO.setBody(body);

	}
	
	@BeforeEach
	void setUpRegistrationConverter() {
		registrationResponse = new RegistrationResponse();
		registrationResponse.setUsername("1234");
		registrationResponse.setPassword("1234");
	}
	
	@BeforeEach
	void setUpTraineeProfileResponse() {
		traineeProfileResponse = new TraineeProfileResponse();
		traineeProfileResponse.setFirstName("12");
		traineeProfileResponse.setLastName("34");
		traineeProfileResponse.setActive(true);
		traineeProfileResponse.setAddress("hyd");
	}
	
	@BeforeEach
	void setUpUpdateTraineeDTO() {
		updateTraineeDTO = new UpdateTraineeDTO();
		updateTraineeDTO.setAddress("hyd");	
		updateTraineeDTO.setEmail("123@gmail.com");
		updateTraineeDTO.setFirstName("12");
		updateTraineeDTO.setLastName("34");
		updateTraineeDTO.setUsername("1234");
		updateTraineeDTO.setIsActive(true);
		updateTraineeDTO.setDateOfBirth(new Date());
	}
	
	@BeforeEach
	void setUpUpdatedTraineeResponse() {
		updatedTraineeResponse = new UpdatedTraineeResponse();
		updatedTraineeResponse.setActive(true);
		updatedTraineeResponse.setAddress("hyd");
		updatedTraineeResponse.setEmail("123@gmail.com");
		updatedTraineeResponse.setFirstName("12");
		updatedTraineeResponse.setLastName("34");
		updatedTraineeResponse.setTrainers(List.of());
		updatedTraineeResponse.setUsername("1234");
	}
	
	@BeforeEach
	void setUpTrainerResponse() {
		trainerResponse = new TrainerResponse();
		trainerResponse.setFirstName("34");
		trainerResponse.setLastName("12");
		trainerResponse.setUsername("3412");
		trainerResponse.setSpecialization("zumba");
	}


}
