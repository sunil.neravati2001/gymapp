package com.epam.gym.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.gym.dao.TraineeRepository;
import com.epam.gym.dao.TrainerRepository;
import com.epam.gym.dao.TrainingRepository;
import com.epam.gym.dao.UserRepository;
import com.epam.gym.dto.NotificationDTO;
import com.epam.gym.dto.TraineeDTO;
import com.epam.gym.dto.UpdateTraineeDTO;
import com.epam.gym.dto.response.RegistrationResponse;
import com.epam.gym.dto.response.TraineeProfileResponse;
import com.epam.gym.dto.response.TrainerResponse;
import com.epam.gym.dto.response.UpdatedTraineeResponse;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.TrainingType;
import com.epam.gym.entity.User;
import com.epam.gym.exception.GymException;
import com.epam.gym.service.impl.NotificationProducer;
import com.epam.gym.service.impl.TraineeServiceImpl;
import com.epam.gym.utility.CredentialGenerator;
import com.epam.gym.utility.NotificationConverter;
import com.epam.gym.utility.RegistrationConverter;
import com.epam.gym.utility.TraineeConverter;
import com.epam.gym.utility.TrainerConverter;
import com.epam.gym.utility.UserConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(MockitoExtension.class)
class TraineeServiceTest {
	
	private Trainee trainee;
	
	private User traineeUser;
	
	private User trainerUser;
	
	private TraineeDTO traineeDTO;
	
	private NotificationDTO notificationDTO;
	
	private RegistrationResponse registrationResponse;
	
	private TraineeProfileResponse traineeProfileResponse;
	
	private UpdateTraineeDTO updateTraineeDTO;
	
	private UpdatedTraineeResponse updatedTraineeResponse;
	
	private Trainee traineeWithTrainer;
	
	private Trainer trainer;
	
	private TrainingType trainingType;
	
	private TrainerResponse trainerResponse;
	
	@Mock
	private UserRepository userRepository;
	
	@Mock
	private TraineeRepository traineeRepository;
	
	@Mock
	private TrainingRepository trainingRepository;
	
	@Mock
	private TrainerRepository trainerRepository;
	
	@Mock
	private NotificationProducer notificationProducer;
	
	@Mock
	private PasswordEncoder encoder;
	
	@Mock
	private CredentialGenerator generator;
	
	@Mock
	private NotificationConverter notificationConverter;
	
	@Mock
	private RegistrationConverter registrationConverter;
	
	@Mock
	private UserConverter userConverter;
	
	@Mock
	private TraineeConverter traineeConverter;
	
	@Mock
	private TrainerConverter trainerConverter;
	
	@InjectMocks
	private TraineeServiceImpl traineeServiceImpl;

	@Test
	void testTraineeRegistration() throws JsonProcessingException {
		Mockito.when(userRepository.findByUsernameStartsWith("1234")).thenReturn(List.of());
		Mockito.when(generator.generateUniqueUsername("12", "34", List.of())).thenReturn("1234");
		Mockito.when(generator.generatePassword()).thenReturn("1234");
		Mockito.when(encoder.encode("1234")).thenReturn("1234");
		Mockito.when(userConverter.getUserFromTraineeDTO(traineeDTO, "1234", "1234")).thenReturn(traineeUser);
		Mockito.when(traineeConverter.getTrainee(traineeDTO, traineeUser)).thenReturn(trainee);
		Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);
		Mockito.when(notificationConverter.getNotificationDTO(traineeUser, "1234")).thenReturn(notificationDTO);
		Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);
		Mockito.when(registrationConverter.getRegistrationResponse("1234", "1234")).thenReturn(registrationResponse);
		assertEquals(registrationResponse, traineeServiceImpl.traineeRegistration(traineeDTO));
		Mockito.verify(userRepository).findByUsernameStartsWith("1234");
		Mockito.verify(generator).generateUniqueUsername("12", "34", List.of());
		Mockito.verify(generator).generatePassword();
		Mockito.verify(encoder).encode("1234");
		Mockito.verify(traineeConverter).getTrainee(traineeDTO, traineeUser);
		Mockito.verify(userConverter).getUserFromTraineeDTO(traineeDTO, "1234", "1234");
		Mockito.verify(traineeRepository).save(trainee);
		Mockito.verify(notificationConverter).getNotificationDTO(traineeUser, "1234");
		Mockito.verify(notificationProducer).sendNotification(notificationDTO);
		Mockito.verify(registrationConverter).getRegistrationResponse("1234", "1234");
	}
	
	@Test
	void testGetTraineeProfileResponse() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(traineeConverter.getTraineeProfileResponse(trainee, trainee.getUser(), List.of())).thenReturn(traineeProfileResponse);
		assertEquals(traineeProfileResponse, traineeServiceImpl.getTraineeProfileDetails("1234"));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(traineeConverter).getTraineeProfileResponse(trainee, trainee.getUser(), List.of());
	}
	
	@Test
	void testGetTraineeProfileResponseWithInvalidTrainee() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> traineeServiceImpl.getTraineeProfileDetails("1234"));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
	}
	
	@Test
	void testGetTraineeProfileWithTrainer() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(traineeWithTrainer));
		Mockito.when(trainerConverter.getTrainerResponse(any(),any())).thenReturn(trainerResponse);
		Mockito.when(traineeConverter.getTraineeProfileResponse(any(),any(),any())).thenReturn(traineeProfileResponse);
		assertEquals(traineeProfileResponse, traineeServiceImpl.getTraineeProfileDetails("1234"));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(trainerConverter).getTrainerResponse(any(),any());
		Mockito.verify(traineeConverter).getTraineeProfileResponse(any(), any(), any());
	}
	
	@Test
	void testUpdateTraineeDetails() throws JsonProcessingException {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(userConverter.getUserFromUpdateTraineeDTO(any(), any())).thenReturn(traineeUser);
		Mockito.when(traineeConverter.getUpdatedTraineeResponse(any(), any(), any())).thenReturn(updatedTraineeResponse);
		Mockito.when(notificationConverter.getNotificationDTO(trainee)).thenReturn(notificationDTO);
		Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);
		assertEquals(updatedTraineeResponse, traineeServiceImpl.updateTraineeDetails(updateTraineeDTO));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(userConverter).getUserFromUpdateTraineeDTO(any(), any());
		Mockito.verify(traineeConverter).getUpdatedTraineeResponse(any(), any(), any());
		Mockito.verify(notificationConverter).getNotificationDTO(trainee);
		Mockito.verify(notificationProducer).sendNotification(notificationDTO);
	}
	
	@Test
	void testUpdateTraineeDetailsWithInvalidTrainee() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> traineeServiceImpl.updateTraineeDetails(updateTraineeDTO));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
	}
	
	@Test
	void testUpdateTrianeeDetailsWithTrainer() throws JsonProcessingException {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(traineeWithTrainer));
		Mockito.when(userConverter.getUserFromUpdateTraineeDTO(any(), any())).thenReturn(traineeUser);
		Mockito.when(traineeConverter.getUpdatedTraineeResponse(any(), any(), any())).thenReturn(updatedTraineeResponse);
		Mockito.when(notificationConverter.getNotificationDTO(traineeWithTrainer)).thenReturn(notificationDTO);
		Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);
		assertEquals(updatedTraineeResponse, traineeServiceImpl.updateTraineeDetails(updateTraineeDTO));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(userConverter).getUserFromUpdateTraineeDTO(any(), any());
		Mockito.verify(traineeConverter).getUpdatedTraineeResponse(any(), any(), any());
		Mockito.verify(notificationConverter).getNotificationDTO(traineeWithTrainer);
		Mockito.verify(notificationProducer).sendNotification(notificationDTO);
	}
	
	@Test
	void testDeleteTrainee() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.doNothing().when(traineeRepository).delete(trainee);
		traineeServiceImpl.deleteTrainee("1234");
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(traineeRepository).delete(trainee);
	}
	
	@Test
	void testDeleteInvalidTrainee() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> traineeServiceImpl.deleteTrainee("1234"));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
	}
	
	@Test
	void testDeleteTraineeWithTrainer() {
		Set<Trainee> trainees = new HashSet<>();
		trainees.add(traineeWithTrainer);
		trainer.setTrainees(trainees);
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(traineeWithTrainer));
		Mockito.doNothing().when(traineeRepository).delete(any());
		traineeServiceImpl.deleteTrainee("1234");
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(traineeRepository).delete(any());
		Mockito.verify(traineeRepository).findByUserUsername("1234");
	}
	
	@Test
	void testNotAssignedTrainers() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(trainerRepository.findByTraineesNotContaining(trainee)).thenReturn(List.of());
		assertEquals(List.of(), traineeServiceImpl.fetchNotAssignedTrainers("1234"));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(trainerRepository).findByTraineesNotContaining(trainee);
	}
	
	@Test
	void testNotAssignedTrainersWithNotAssignedTrainers() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(trainerRepository.findByTraineesNotContaining(trainee)).thenReturn(List.of(trainer));
		Mockito.when(trainerConverter.getTrainerResponse(any(), any())).thenReturn(trainerResponse);		
		assertEquals(List.of(trainerResponse), traineeServiceImpl.fetchNotAssignedTrainers("1234"));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(trainerRepository).findByTraineesNotContaining(trainee);
		Mockito.verify(trainerConverter).getTrainerResponse(any(), any());
	}
	
	@Test
	void testNotAssignedTrainersWithInvalidUser() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> traineeServiceImpl.fetchNotAssignedTrainers("1234"));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
	}
	
	@Test
	void testUpdateTrainers() {
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainee));
		Mockito.when(trainerRepository.findByUserUsernameIn(List.of("Raju"))).thenReturn(new HashSet<>());
		assertEquals(List.of(),traineeServiceImpl.updateTrainers("1234",List.of("Raju")));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(trainerRepository).findByUserUsernameIn(List.of("Raju"));
	}
	
	@Test
	void testUpdateTrainersWithExistingTrainers() {
		Set<Trainee> trainees = new HashSet<>();
		trainees.add(traineeWithTrainer);
		trainer.setTrainees(trainees);
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(traineeWithTrainer));
		Mockito.when(trainerRepository.findByUserUsernameIn(List.of("1234"))).thenReturn(new HashSet<>());
		Mockito.when(trainingRepository.findByTrainerAndTrainee(any(), any())).thenReturn(List.of());
		Mockito.doNothing().when(trainingRepository).deleteAll(any());
		assertEquals(List.of(),traineeServiceImpl.updateTrainers("1234",List.of("1234")));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(trainerRepository).findByUserUsernameIn(List.of("1234"));
		Mockito.verify(trainingRepository).findByTrainerAndTrainee(any(), any());
		Mockito.verify(trainingRepository).deleteAll(any());
	}
	
	@Test
	void testUpdateTrainersWithNewTrainers() {
		Set<Trainee> trainees = new HashSet<>();
		trainees.add(traineeWithTrainer);
		trainer.setTrainees(trainees);
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.of(traineeWithTrainer));
		Mockito.when(trainerRepository.findByUserUsernameIn(List.of("1234"))).thenReturn(new HashSet<>(List.of(trainer)));
		Mockito.when(trainerConverter.getTrainerResponse(any(), any())).thenReturn(trainerResponse);
		assertEquals(trainerResponse,traineeServiceImpl.updateTrainers("1234",List.of("1234")).get(0));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
		Mockito.verify(trainerRepository).findByUserUsernameIn(List.of("1234"));
		Mockito.verify(trainerConverter).getTrainerResponse(any(), any());
	}
	
	@Test
	void testUpdateInvalidTrainer() {
		List<String> trainers = List.of("Raju");
		Mockito.when(traineeRepository.findByUserUsername("1234")).thenReturn(Optional.empty());
		assertThrows(GymException.class, () -> traineeServiceImpl.updateTrainers("1234",trainers));
		Mockito.verify(traineeRepository).findByUserUsername("1234");
	}
	
	@BeforeEach
	void setUpTrainingType() {
		trainingType = new TrainingType();
		trainingType.setId(1);
		trainingType.setTrainingTypeName("zumba");
	}
	
	@BeforeEach
	void setUpTrainee() {
		trainee = Trainee.builder().id(1).address("hyd").user(traineeUser).trainers(new HashSet<>()).build();
	}
	
	@BeforeEach
	void setUpTraineeWithTrainer(){
		traineeWithTrainer = Trainee.builder().id(1).address("hyd").user(traineeUser).trainers(new HashSet<>(List.of(trainer))).build();
	}
	
	@BeforeEach
	void setUpTrainer() {
		trainer = new Trainer();
		trainer.setUser(trainerUser);
		trainer.setSpecialization(trainingType);
	}
	
	@BeforeEach
	void setUpTraineeUser() {
		traineeUser = User.builder().createdDate(new Date()).email("123@gmail.com").firstName("12").lastName("34").username("1234").password("1234").isActive(true).id(1).build();
	}
	
	@BeforeEach
	void setUpTrainerUser() {
		trainerUser = User.builder().createdDate(new Date()).email("1234@gmail.com").firstName("34").lastName("12").username("3421").password("1234").isActive(true).id(2).build();
	}
	
	@BeforeEach
	void setUpTraineeDTO() {
		traineeDTO = new TraineeDTO();
		traineeDTO.setAddress("hyd");
		traineeDTO.setEmail("123@gmail.com");
		traineeDTO.setFirstName("12");
		traineeDTO.setLastName("34");
	}
	
	@BeforeEach
	void setUpNotificationDTO() {
		
		Map<String,String> body = new LinkedHashMap<>();
		body.put("username", "1234");
		body.put("password", "1234");
		
		NotificationDTO notificationDTO =  new NotificationDTO();
		notificationDTO.setToEmails(List.of("123@gmail.com"));
		notificationDTO.setSubject("REGISTRATION");
		notificationDTO.setBody(body);

	}
	
	@BeforeEach
	void setUpRegistrationConverter() {
		registrationResponse = new RegistrationResponse();
		registrationResponse.setUsername("1234");
		registrationResponse.setPassword("1234");
	}
	
	@BeforeEach
	void setUpTraineeProfileResponse() {
		traineeProfileResponse = new TraineeProfileResponse();
		traineeProfileResponse.setFirstName("12");
		traineeProfileResponse.setLastName("34");
		traineeProfileResponse.setActive(true);
		traineeProfileResponse.setAddress("hyd");
	}
	
	@BeforeEach
	void setUpUpdateTraineeDTO() {
		updateTraineeDTO = new UpdateTraineeDTO();
		updateTraineeDTO.setAddress("hyd");	
		updateTraineeDTO.setEmail("123@gmail.com");
		updateTraineeDTO.setFirstName("12");
		updateTraineeDTO.setLastName("34");
		updateTraineeDTO.setUsername("1234");
		updateTraineeDTO.setIsActive(true);
		updateTraineeDTO.setDateOfBirth(new Date());
	}
	
	@BeforeEach
	void setUpUpdatedTraineeResponse() {
		updatedTraineeResponse = new UpdatedTraineeResponse();
		updatedTraineeResponse.setActive(true);
		updatedTraineeResponse.setAddress("hyd");
		updatedTraineeResponse.setEmail("123@gmail.com");
		updatedTraineeResponse.setFirstName("12");
		updatedTraineeResponse.setLastName("34");
		updatedTraineeResponse.setTrainers(List.of());
		updatedTraineeResponse.setUsername("1234");
	}
	
	@BeforeEach
	void setUpTrainerResponse() {
		trainerResponse = new TrainerResponse();
		trainerResponse.setFirstName("34");
		trainerResponse.setLastName("12");
		trainerResponse.setUsername("3412");
		trainerResponse.setSpecialization("zumba");
	}

}
