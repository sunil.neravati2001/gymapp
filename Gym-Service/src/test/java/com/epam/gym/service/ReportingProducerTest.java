package com.epam.gym.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import com.epam.gym.dto.TrainingReportDTO;
import com.epam.gym.service.impl.ReportingProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
class ReportingProducerTest {

	@InjectMocks
    private ReportingProducer reportingProducer;

    @Mock
    private KafkaTemplate<String, String> kafkaTemplate;

    @Mock
    private ObjectMapper objectMapper;

    @Test
    void testSendReport() throws JsonProcessingException {
    	TrainingReportDTO trainingReportDTO = new TrainingReportDTO();
        String expectedJson = "{\"key\": \"value\"}"; 
        when(objectMapper.writeValueAsString(trainingReportDTO)).thenReturn(expectedJson);
        reportingProducer.sendReport(trainingReportDTO);
        reportingProducer.close();
        verify(kafkaTemplate, times(1)).send(any(), any());
    }

    @Test
    void testSendReportJsonProcessingError() throws JsonProcessingException {
    	TrainingReportDTO trainingReportDTO = new TrainingReportDTO();
        when(objectMapper.writeValueAsString(trainingReportDTO)).thenThrow(JsonProcessingException.class);
        reportingProducer.close();
        assertThrows(JsonProcessingException.class, () -> reportingProducer.sendReport(trainingReportDTO));
    }

    @Test
    void testSendReportKafkaError() throws JsonProcessingException {
    	TrainingReportDTO trainingReportDTO = new TrainingReportDTO();
        String expectedJson = "{\"key\": \"value\"}"; 
        when(objectMapper.writeValueAsString(trainingReportDTO)).thenReturn(expectedJson);
        doThrow(new RuntimeException("Test exception")).when(kafkaTemplate).send(anyString(), anyString());
        reportingProducer.close();
        assertDoesNotThrow(() -> reportingProducer.sendReport(trainingReportDTO));
    }

    
    @Test
    void testSendReportWithNullKafka() throws JsonProcessingException {
    	TrainingReportDTO trainingReportDTO = new TrainingReportDTO();
        String expectedJson = "{\"key\": \"value\"}"; 
        when(objectMapper.writeValueAsString(trainingReportDTO)).thenReturn(expectedJson);
        doThrow(new RuntimeException("Test exception")).when(kafkaTemplate).send(anyString(), anyString());
        kafkaTemplate.destroy();
        reportingProducer.close();
        assertDoesNotThrow(() -> reportingProducer.sendReport(trainingReportDTO));
    }
}


