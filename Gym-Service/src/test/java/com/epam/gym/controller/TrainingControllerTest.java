package com.epam.gym.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gym.dto.TrainingDTO;
import com.epam.gym.dto.response.TrainingResponse;
import com.epam.gym.service.TrainingService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TrainingController.class)
class TrainingControllerTest {

	private TrainingDTO trainingDTO;
	
	private TrainingResponse trainingResponse;
	
	@MockBean
	private TrainingService trainingService;
	
	@Autowired
	private MockMvc mockMvc;
	

    @Test
    void testAddTraining() throws Exception {
    	
    	Mockito.doNothing().when(trainingService).addTraining(trainingDTO);

        mockMvc.perform(post("/gym/training/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(trainingDTO)))
                .andExpect(status().isOk());

        Mockito.verify(trainingService, times(1)).addTraining(any(TrainingDTO.class));
    }
    
    @Test
    void testGetTraineeTrainings() throws Exception {
        String username = "testUsername";
        Date periodFrom = null;
        Date periodTo = null;
        String trainerName = "testTrainerName";
        String trainingType = "testTrainingType";
        
        List<TrainingResponse> trainingResponses = new ArrayList<>(); 
        trainingResponses.add(trainingResponse);
        
        Mockito.when(trainingService.getTraineeTrainings(username, periodFrom, periodTo, trainerName, trainingType))
            .thenReturn(trainingResponses);

        mockMvc.perform(get("/gym/training/trainee")
                .param("username", username)
                .param("trainerName", trainerName)
                .param("trainingType", trainingType))
                .andExpect(status().isOk());

        Mockito.verify(trainingService, times(1))
            .getTraineeTrainings(username, periodFrom, periodTo, trainerName, trainingType);
    }
    
    @Test
    void testGetTrainerTrainings() throws Exception {
        String username = "testUsername";
        Date periodFrom = null;
        Date periodTo = null;
        String traineeName = "testTraineeName";
        
        List<TrainingResponse> trainingResponses = new ArrayList<>(); 
        trainingResponses.add(trainingResponse);
        
        Mockito.when(trainingService.getTrainerTrainings(username, periodFrom, periodTo, traineeName))
            .thenReturn(trainingResponses);

        mockMvc.perform(get("/gym/training/trainer")
                .param("username", username)
                .param("traineeName", traineeName))
                .andExpect(status().isOk());

        Mockito.verify(trainingService, times(1))
            .getTrainerTrainings(username, periodFrom, periodTo, traineeName);
    }
	
	
	
	@BeforeEach
	void setUpTrainingResponse() {
		trainingResponse = new TrainingResponse();
		trainingResponse.setTrainingDate(new Date());
		trainingResponse.setTrainingDuration(3);
		trainingResponse.setTrainingName("zumba-training");
		trainingResponse.setTrainingType("zumba");
		trainingResponse.setUsername("1234");
	}
	
	@BeforeEach
	void setUpTrainingDTO() {
		trainingDTO = new TrainingDTO();
		trainingDTO.setTraineeUsername("1234");
		trainingDTO.setTrainerUsername("3412");
		trainingDTO.setTrainingType("zumba");
		trainingDTO.setDuration(3);
		trainingDTO.setTrainingName("zumba-training");
		trainingDTO.setTrainingDate(new Date());
	}

}
