package com.epam.gym.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gym.exception.GymException;
import com.epam.gym.service.LoginService;

@WebMvcTest(LoginController.class)
class LoginControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private LoginService loginService;
	
	@Test
	void testLogin() throws Exception {
		
		Mockito.doNothing().when(loginService).login("testUser", "testPassword");
        mockMvc.perform(get("/gym/login")
                .param("userName", "testUser")
                .param("password", "testPassword"))
            .andExpect(status().isOk());
        Mockito.verify(loginService).login("testUser", "testPassword");
        
	}
	
	@Test
	void testFailureLogin() throws Exception {
		Mockito.doThrow(GymException.class).when(loginService).login("testUser", "testPassword");
        mockMvc.perform(get("/gym/login")
                .param("userName", "testUser")
                .param("password", "testPassword"))
            .andExpect(status().isBadRequest());
        Mockito.verify(loginService).login("testUser", "testPassword");
        
	}
	
    @Test
    void testUpdatePassword() throws Exception {
    	Mockito.doNothing().when(loginService).changePassword("testUser", "oldTestPassword", "newTestPassword");
        mockMvc.perform(get("/gym/updatepassword")
                .param("userName", "testUser")
                .param("oldPassword", "oldTestPassword")
                .param("newPassword", "newTestPassword"))
            .andExpect(status().isOk());
        Mockito.verify(loginService).changePassword("testUser", "oldTestPassword", "newTestPassword");
    }
    
    @Test
    void testUpdatePasswordFailure() throws Exception {
    	Mockito.doThrow(RuntimeException.class).when(loginService).changePassword("testUser", "oldTestPassword", "newTestPassword");
        mockMvc.perform(get("/gym/updatepassword")
                .param("userName", "testUser")
                .param("oldPassword", "oldTestPassword")
                .param("newPassword", "newTestPassword"))
            .andExpect(status().isInternalServerError());
        Mockito.verify(loginService).changePassword("testUser", "oldTestPassword", "newTestPassword");
    }

}
