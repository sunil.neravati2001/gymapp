package com.epam.identityservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.identityservice.entity.BruteForceProtectedUser;

@Repository
public interface BruteForceProtectedUserRepository extends JpaRepository<BruteForceProtectedUser, Integer> {
	
	Optional<BruteForceProtectedUser> findByUsername(String username);

}
