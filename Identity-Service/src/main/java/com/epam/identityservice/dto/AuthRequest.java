package com.epam.identityservice.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthRequest {
	
	@NotBlank(message = "username should not be blank")
    private String username;
	
	@NotBlank(message = "password should not be blank")
    private String password;

}
