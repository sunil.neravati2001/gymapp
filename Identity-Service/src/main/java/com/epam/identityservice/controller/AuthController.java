package com.epam.identityservice.controller;


import com.epam.identityservice.dto.TokenDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.identityservice.dto.AuthRequest;
import com.epam.identityservice.exception.AuthException;
import com.epam.identityservice.service.AuthService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/auth")
@Slf4j
public class AuthController {
	
    @Autowired
    private AuthService service;

    @Autowired
    private AuthenticationManager authenticationManager;


    @PostMapping("/token")
    public ResponseEntity<TokenDTO> getToken(@RequestBody @Valid AuthRequest authRequest) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        if (authenticate.isAuthenticated()) {
            return new ResponseEntity<>(new TokenDTO(service.generateToken(authRequest.getUsername())),HttpStatus.OK);
        } else {
            throw new AuthException("invalid access");
        }
    }

    @GetMapping("/validate")
    public ResponseEntity<Void> validateToken(@RequestParam String token) {
    	log.info(token);
        try {
			service.validateToken(token);
			log.info("validateddd");
		} catch (Exception e) {
			log.info("Invalid Access, {} ",e.getMessage());
			throw new AuthException("UnAuthorised Access to Application");
		}
        log.info("Exit validate token");
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}
