package com.epam.notification.service.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.epam.notification.dto.NotificationDTO;
import com.epam.notification.dto.Status;
import com.epam.notification.dto.Subject;
import com.epam.notification.entity.Notification;
import com.epam.notification.repository.NotificationRepository;
import com.epam.notification.service.NotificationService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationServiceImpl implements NotificationService {
	
	private final JavaMailSender javaMailSender;
	private final NotificationRepository notificationRepository;
	
	@Value("${spring.mail.username}")
	private String fromEmail;

	@Override
	public void sendNotification(NotificationDTO notificationDTO) {
		
		log.info("Entered send notification method, notificationDTO : {}",notificationDTO);
		
		String status;
		String remarks;		
		SimpleMailMessage message = new SimpleMailMessage();
		
		String body = notificationDTO.getBody().entrySet().stream().map(entry -> entry.getKey() + ": " + entry.getValue()) .collect(Collectors.joining("\n"));
		
        if (notificationDTO.getToEmails() != null && !notificationDTO.getToEmails().isEmpty()) {
            String[] toArray = notificationDTO.getToEmails().toArray(new String[notificationDTO.getToEmails().size()]);
            message.setTo(toArray);
        }
        
        if (notificationDTO.getCcEmails() != null && !notificationDTO.getCcEmails().isEmpty()) {
            String[] ccArray = notificationDTO.getCcEmails().toArray(new String[notificationDTO.getCcEmails().size()]);
            message.setCc(ccArray);
        }
        
        message.setSubject(Subject.valueOf(notificationDTO.getSubject()).getMessage());
        message.setText(body);
        message.setFrom(fromEmail);
       
        try{
        	javaMailSender.send(message);
        	status = Status.SENT.toString();
        	remarks="Mail sent Successfully";
        }
        catch (Exception e) {
			status = Status.FAILED.toString();
			remarks = e.getMessage();
		}
        
        notificationRepository.save(getNotification(notificationDTO, status, remarks));
        
	}

	private Notification getNotification(NotificationDTO notificationDTO, String status, String remarks) {
		
		Notification notification = new Notification();
        notification.setBody(notificationDTO.getBody());
        notification.setToEmails(notificationDTO.getToEmails());
        notification.setCcEmails(notificationDTO.getCcEmails());
        notification.setFromEmail(fromEmail);
        notification.setRemarks(remarks);
        notification.setStatus(status);
        notification.setSubject(Subject.valueOf(notificationDTO.getSubject()).getMessage());
		return notification;
	}

}
