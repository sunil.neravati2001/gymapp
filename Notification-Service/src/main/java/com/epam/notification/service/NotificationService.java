package com.epam.notification.service;

import com.epam.notification.dto.NotificationDTO;

public interface NotificationService {
	
	void sendNotification(NotificationDTO notificationDTO);

}
