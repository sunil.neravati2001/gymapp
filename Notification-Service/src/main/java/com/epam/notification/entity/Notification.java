package com.epam.notification.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
	
	@Id
	private String id;
	private String fromEmail;
	private List<String> toEmails;
	private List<String> ccEmails;
	private Map<String,String> body;
	private String status;
	private String remarks;
	private String subject;

}
