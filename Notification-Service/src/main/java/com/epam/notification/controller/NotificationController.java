package com.epam.notification.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.notification.dto.NotificationDTO;
import com.epam.notification.dto.NotificationResponse;
import com.epam.notification.service.NotificationService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/notification")
@Slf4j
public class NotificationController {
	
	private final NotificationService notificationService;
	
	@PostMapping("/send")
	public ResponseEntity<NotificationResponse> sendMail(@RequestBody @Valid NotificationDTO notificationDTO){
		log.info("Entered send mail method, NotificationDTO : {}",notificationDTO);
		notificationService.sendNotification(notificationDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
