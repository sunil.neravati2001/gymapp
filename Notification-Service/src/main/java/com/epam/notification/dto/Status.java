package com.epam.notification.dto;

import lombok.ToString;

@ToString
public enum Status {
	NEW,FAILED,SENT
}
