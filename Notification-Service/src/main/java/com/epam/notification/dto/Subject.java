package com.epam.notification.dto;

public enum Subject {

	REGISTRATION("USER REGISTERED SUCCESSFULLY"), UPDATE_TRAINEE("TRAINEE DETAILS UPDATED SUCCESSFULLY"),
	UPDATE_TRAINER("TRAINER DETAILS UPDATED SUCCESSFULLY"),
	TRAINING_REGISTRATION("TRAINING DETAILS ADDED SUCCESSFULLY");

	private final String message;

	Subject(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
