package com.epam.notification.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.epam.notification.dto.NotificationDTO;
import com.epam.notification.entity.Notification;
import com.epam.notification.repository.NotificationRepository;
import com.epam.notification.service.impl.NotificationServiceImpl;

@ExtendWith(MockitoExtension.class)
class NotificationServiceTest {

	private NotificationDTO notificationDTO;

	@Mock
	private JavaMailSender javaMailSender;

	@Mock
	private NotificationRepository notificationRepository;

	@InjectMocks
	private NotificationServiceImpl notificationService;

	@Value("${spring.mail.username}")
	private String fromEmail;

	@BeforeEach
	void setup() {
		notificationDTO = new NotificationDTO();
		notificationDTO.setToEmails(Collections.singletonList("to@example.com"));
		notificationDTO.setCcEmails(Collections.singletonList("cc@example.com"));
		notificationDTO.setSubject("REGISTRATION");
		notificationDTO.setBody(Collections.singletonMap("key", "value"));

	}

	@Test
	void testSendNotificationSuccess() {

		Mockito.doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));

		notificationService.sendNotification(notificationDTO);

		Mockito.verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
		Mockito.verify(notificationRepository, times(1)).save(any(Notification.class));

	}
	
	@Test
	void testSendNotificationWithoutCCemail() {
		
		notificationDTO.setCcEmails(null);

		Mockito.doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));

		notificationService.sendNotification(notificationDTO);

		Mockito.verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
		Mockito.verify(notificationRepository, times(1)).save(any(Notification.class));

	}
	
	@Test
	void testSendNotificationWithEmptyCCemail() {
		
		notificationDTO.setCcEmails(new ArrayList<>());

		Mockito.doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));

		notificationService.sendNotification(notificationDTO);

		Mockito.verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
		Mockito.verify(notificationRepository, times(1)).save(any(Notification.class));

	}
	
	@Test
	void testSendNotificationWithoutToemail() {
		
		notificationDTO.setToEmails(null);

		Mockito.doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));

		notificationService.sendNotification(notificationDTO);

		Mockito.verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
		Mockito.verify(notificationRepository, times(1)).save(any(Notification.class));

	}
	
	@Test
	void testSendNotificationWithEmptyToemail() {
		
		notificationDTO.setToEmails(new ArrayList<>());

		Mockito.doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));

		notificationService.sendNotification(notificationDTO);

		Mockito.verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
		Mockito.verify(notificationRepository, times(1)).save(any(Notification.class));

	}

	@Test
	void testSendNotificationFailure() {

		Mockito.doThrow(RuntimeException.class).when(javaMailSender).send(any(SimpleMailMessage.class));

		notificationService.sendNotification(notificationDTO);

		Mockito.verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
		Mockito.verify(notificationRepository, times(1)).save(any(Notification.class));

	}

}
