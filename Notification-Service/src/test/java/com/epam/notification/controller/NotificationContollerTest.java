package com.epam.notification.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.notification.dto.NotificationDTO;
import com.epam.notification.dto.NotificationResponse;
import com.epam.notification.dto.Status;
import com.epam.notification.repository.NotificationRepository;
import com.epam.notification.service.NotificationService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(NotificationController.class)
class NotificationContollerTest {
	
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NotificationService notificationService;
    
    @MockBean
    private NotificationRepository notificationRepository;

    private NotificationDTO notificationDTO;
    
    private NotificationResponse notificationResponse;
    
    @Test
    void testSendMail() throws Exception {
    	
    	Mockito.doNothing().when(notificationService).sendNotification(any(NotificationDTO.class));
    	
        mockMvc.perform(post("/notification/send").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(notificationDTO))).andExpect(status().isOk());
        
        Mockito.verify(notificationService).sendNotification(any(NotificationDTO.class));
    }

    @BeforeEach
    void setNotificationDTO() {
        notificationDTO = new NotificationDTO();
        notificationDTO.setToEmails(Collections.singletonList("to@example.com"));
        notificationDTO.setCcEmails(Collections.singletonList("cc@example.com"));
        notificationDTO.setSubject("REGISTRATION");
        notificationDTO.setBody(Collections.singletonMap("key", "value"));
    }
    
    @BeforeEach
    void setNotificationResponse() {
        notificationResponse = new NotificationResponse();
        notificationResponse.setRemarks("Mail sent Successfully");
        notificationResponse.setStatus(Status.SENT.toString());
    }
}
