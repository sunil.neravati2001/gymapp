package com.epam.reporting.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.epam.reporting.entity.TrainingSummary;

@Repository
public interface ReportingRepository extends MongoRepository<TrainingSummary, String> {

}
