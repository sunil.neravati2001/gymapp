package com.epam.reporting.dto;

import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TrainingSummaryResponse {
	
	private String username;
	private String firstName;
	private String lastName;
	private TrainerStatusDTO status;
	private Map<Integer,Map<Integer,Map<Integer,Integer>>> duration;

}
