package com.epam.reporting.dto;

import lombok.ToString;

@ToString
public enum TrainerStatusDTO {
	ACTIVE,INACTIVE
}
