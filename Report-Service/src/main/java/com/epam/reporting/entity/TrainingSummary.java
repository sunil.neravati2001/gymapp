package com.epam.reporting.entity;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TrainingSummary {

	@Id
	private String username;
	private String firstName;
	private String lastName;
	private TrainerStatus status;
	private Map<Integer,Map<Integer,Map<Integer,Integer>>> duration;

}
