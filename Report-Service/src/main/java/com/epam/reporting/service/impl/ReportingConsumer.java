package com.epam.reporting.service.impl;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.service.ReportingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@Service
public class ReportingConsumer {
	
	private final ReportingService reportingService;
	private final ObjectMapper objectMapper;
	
	@KafkaListener(topics = "${reporting.topic}" ,groupId = "${spring.kafka.consumer.group-id}")
	public void updateReport(String message) throws JsonProcessingException {
		log.info("Entered update report method, trainingReportDTO : {}",message);
		TrainingReportDTO trainingReportDTO = objectMapper.readValue(message, TrainingReportDTO.class);
		reportingService.updateReport(trainingReportDTO);
	}

}
