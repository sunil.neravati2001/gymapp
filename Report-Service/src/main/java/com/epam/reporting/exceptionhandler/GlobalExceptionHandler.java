package com.epam.reporting.exceptionhandler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.reporting.dto.ExceptionResponse;
import com.epam.reporting.exception.ReportException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ExceptionResponse> argumentValidationException(MethodArgumentNotValidException errors,
			WebRequest request) {
		StringBuilder error = new StringBuilder();
		errors.getAllErrors().forEach(er -> error.append(er.getDefaultMessage() + " , "));
		log.error(error.toString());
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				error.toString(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ExceptionResponse> runtimeExceptionHandler(RuntimeException message, WebRequest request) {
		log.error("Something went wrong " + message.getMessage());
		return new ResponseEntity<>(
				new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.name(),
						"Something went wrong " + message.getMessage(), request.getDescription(false)),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(ReportException.class)
	public ResponseEntity<ExceptionResponse> reportExceptionalHandler(ReportException message,
			WebRequest request) {
		log.error(message.getMessage());
		return new ResponseEntity<>(
				new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
						message.getMessage(), request.getDescription(false)),
				HttpStatus.BAD_REQUEST);
	}
	
}
