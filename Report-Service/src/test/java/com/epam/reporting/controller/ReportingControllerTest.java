package com.epam.reporting.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.reporting.dao.ReportingRepository;
import com.epam.reporting.dto.TrainerStatusDTO;
import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.dto.TrainingSummaryResponse;
import com.epam.reporting.exception.ReportException;
import com.epam.reporting.service.ReportingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ReportingController.class)
class ReportingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReportingService reportingService;

    @MockBean
    private ReportingRepository reportingRepository;

    private TrainingReportDTO trainingReportDTO;
    
    private TrainingSummaryResponse trainingSummaryResponse;

    @BeforeEach
    void setTrainingReportDTO() {
        trainingReportDTO = new TrainingReportDTO();
        trainingReportDTO.setUsername("username");
        trainingReportDTO.setFirstName("John");
        trainingReportDTO.setLastName("Doe");
        trainingReportDTO.setIsActive(true);
        trainingReportDTO.setTrainingDate(new Date());
        trainingReportDTO.setTrainingDuration(60);
        trainingReportDTO.setTrainingName("Demo");
    }

    @BeforeEach
    void setTrainingSummaryResponse() {
        trainingSummaryResponse = new TrainingSummaryResponse();
        trainingSummaryResponse.setFirstName("John");
        trainingSummaryResponse.setLastName("Doe");
        trainingSummaryResponse.setUsername("username");
        trainingSummaryResponse.setStatus(TrainerStatusDTO.ACTIVE);
        trainingSummaryResponse.setDuration(Map.of(1,Map.of(2,Map.of(3,4))));
    }
    
    @Test
    void testPostReport() throws JsonProcessingException, Exception {
        Mockito.doNothing().when(reportingService).updateReport(any(TrainingReportDTO.class));

        mockMvc.perform(post("/report").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(trainingReportDTO))).andExpect(status().isOk());

        Mockito.verify(reportingService).updateReport(any(TrainingReportDTO.class));
    }
    
    @Test
    void testPostReportWithRuntimeException() throws JsonProcessingException, Exception {
        Mockito.doThrow(RuntimeException.class).when(reportingService).updateReport(any(TrainingReportDTO.class));

        mockMvc.perform(post("/report").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(trainingReportDTO))).andExpect(status().isInternalServerError());

        Mockito.verify(reportingService).updateReport(any(TrainingReportDTO.class));
    }
    
    @Test
    void testPostReportWithException() throws JsonProcessingException, Exception {
    	
    	trainingReportDTO.setTrainingName(null);

        mockMvc.perform(post("/report").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(trainingReportDTO))).andExpect(status().isBadRequest());

    }

    @Test
    void testGetReport() throws JsonProcessingException, Exception {
        Mockito.when(reportingService.getReport(any(String.class))).thenReturn(trainingSummaryResponse);

        mockMvc.perform(get("/report/{username}","username")).andExpect(status().isOk());

        Mockito.verify(reportingService).getReport(any(String.class));
    }
    
    @Test
    void testGetReportWithException() throws JsonProcessingException, Exception {
        Mockito.when(reportingService.getReport(any(String.class))).thenThrow(ReportException.class);

        mockMvc.perform(get("/report/{username}","username")).andExpect(status().isBadRequest());

        Mockito.verify(reportingService).getReport(any(String.class));
    }
}
