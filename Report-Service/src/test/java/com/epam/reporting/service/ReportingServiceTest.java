package com.epam.reporting.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.reporting.dao.ReportingRepository;
import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.dto.TrainingSummaryResponse;
import com.epam.reporting.entity.TrainerStatus;
import com.epam.reporting.entity.TrainingSummary;
import com.epam.reporting.exception.ReportException;
import com.epam.reporting.service.impl.ReportingServiceImpl;


@ExtendWith(MockitoExtension.class)
class ReportingServiceApplicationTests {

    @Mock
    private ReportingRepository reportingRepository;

    @InjectMocks
    private ReportingServiceImpl reportingService;

    private TrainingReportDTO trainingReportDTO;
    private TrainingReportDTO trainingReportDTO1;
    private TrainingSummary existingSummary;

    @Test
    void testUpdateReport() {
        Mockito.when(reportingRepository.findById("username")).thenReturn(Optional.of(existingSummary));
        Mockito.when(reportingRepository.save(any())).thenReturn(null);
        reportingService.updateReport(trainingReportDTO);
        Mockito.verify(reportingRepository).findById("username");
        Mockito.verify(reportingRepository).save(any());
    }

    @Test
    void testGetReport() {
        existingSummary.setStatus(TrainerStatus.INACTIVE);
        Mockito.when(reportingRepository.findById("username")).thenReturn(Optional.of(existingSummary));
        TrainingSummaryResponse result = reportingService.getReport("username");
        Mockito.verify(reportingRepository).findById("username");
        assertEquals("username", result.getUsername());
        assertEquals(existingSummary.getDuration(), result.getDuration());
    }
    
    @Test
    void testGetReportWithInactiveUser() {
        
    	Mockito.when(reportingRepository.findById("username")).thenReturn(Optional.of(existingSummary));
        TrainingSummaryResponse result = reportingService.getReport("username");
        Mockito.verify(reportingRepository).findById("username");
        assertEquals("username", result.getUsername());
        assertEquals(existingSummary.getDuration(), result.getDuration());
    }

    @Test
    void testGetReportWithInvalidUser() {
    	Mockito.when(reportingRepository.findById("username")).thenReturn(Optional.empty());
       assertThrows(ReportException.class, ()-> reportingService.getReport("username"));
    
    }

    @Test
    void testUpdateReportUserNotFound() {
    	Mockito.when(reportingRepository.findById("username")).thenReturn(Optional.empty());
        reportingService.updateReport(trainingReportDTO);
        Mockito.verify(reportingRepository).findById("username");
        Mockito.verify(reportingRepository).save(any());
    }

    @Test
    void testUpdateReportUserNotFoundWithInactiveStatus() {
    	Mockito.when(reportingRepository.findById("username")).thenReturn(Optional.empty());
        reportingService.updateReport(trainingReportDTO1);
        Mockito.verify(reportingRepository).findById("username");
        Mockito.verify(reportingRepository).save(any());
    }
    
    @BeforeEach
    void setup() {
        trainingReportDTO = new TrainingReportDTO();
        trainingReportDTO.setUsername("username");
        trainingReportDTO.setFirstName("John");
        trainingReportDTO.setLastName("Doe");
        trainingReportDTO.setIsActive(true);
        trainingReportDTO.setTrainingDate(new Date());
        trainingReportDTO.setTrainingDuration(60);

        trainingReportDTO1 = new TrainingReportDTO();
        trainingReportDTO1.setUsername("username");
        trainingReportDTO1.setFirstName("John");
        trainingReportDTO1.setLastName("Doe");
        trainingReportDTO1.setIsActive(false);
        trainingReportDTO1.setTrainingDate(new Date());
        trainingReportDTO1.setTrainingDuration(60);

        existingSummary = new TrainingSummary();
        existingSummary.setUsername("username");
        existingSummary.setStatus(TrainerStatus.ACTIVE);
        existingSummary.setDuration(new HashMap<>());
    }

}
